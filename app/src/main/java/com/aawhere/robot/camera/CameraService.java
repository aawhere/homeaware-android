package com.aawhere.robot.camera;

import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.aawhere.lang.ObjectBuilder;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;

import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Manages accesss to the Android camera features for common situations for a robot.
 * <p/>
 * Camera2 was not used since Tango devices are stuck on SDK 19 and 21 is needed.
 * <p/>
 * Created by aroller on 9/15/15.
 */
public class CameraService {


    private static final String TAG = CameraService.class.getName();
    private Camera camera;
    private SurfaceView surfaceView;
    private File photoDirectory;

    /**
     * Used to construct all instances of CameraService.
     */
    public static class CameraServiceBuilder extends ObjectBuilder<CameraService> {

        private CameraServiceBuilder() {
            super(new CameraService());
        }

        public CameraServiceBuilder photoDirectory(File photoDirectory) {
            building.photoDirectory = photoDirectory;
            return this;
        }


        public CameraServiceBuilder surfaceView(SurfaceView surfaceView) {
            building.surfaceView = surfaceView;
            return this;
        }


        public CameraService build() {
            CameraService built = super.build();
            try {
                built.safeCameraOpen(0);
            } catch (Exception e) {
                Log.w(TAG, "unable to open camera ", e);
            }
            return built;
        }

    }//end Builder

    public static CameraServiceBuilder builder() {
        return new CameraServiceBuilder();
    }


    private boolean safeCameraOpen(int id) {
        boolean qOpened = false;

        try {
            releaseCameraAndPreview();
            camera = Camera.open(id);
            qOpened = (camera != null);
        } catch (Exception e) {
            Log.e(TAG, "failed to open Camera", e);
        }

        return qOpened;
    }

    private void releaseCameraAndPreview() {
        if (camera != null) {
            camera.release();
            camera = null;
        }
    }

    /**
     * Use {@link CameraServiceBuilder} to construct CameraService
     */
    private CameraService() {
    }

    /**
     * Indicates if call to the camera will work.  Clients of the service must call this first or
     * exceptions will be thrown during feature calls.
     *
     * @return
     */
    public boolean available() {
        return this.camera != null;
    }

    /**
     * Asynchronously takes a photo from the front facing camera and writes it to a file.
     *
     * @return
     */
    public ListenableFuture<File> forwardSnapshot() {
        final SettableFuture<File> future = SettableFuture.create();

        final Camera.PictureCallback callback = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {

                File file = new File(photoDirectory, System.currentTimeMillis() + ".jpg");
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    out.write(data);
                    out.close();
                    future.set(file);
                } catch (IOException e) {
                    future.setException(e);
                }
            }
        };
        try {

            this.camera.setPreviewDisplay(surfaceView.getHolder());

            final Camera.Parameters parameters = this.camera.getParameters();
            this.camera.setParameters(parameters);
            this.camera.startPreview();
            this.camera.takePicture(null, null, callback);
            this.camera.stopPreview();
        } catch (IOException e) {
            future.setException(e);
        }

        return future;
    }
}
