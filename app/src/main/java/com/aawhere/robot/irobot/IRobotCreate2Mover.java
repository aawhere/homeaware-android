package com.aawhere.robot.irobot;

import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.aawhere.know.KnowledgeBase;
import com.aawhere.know.KnownAnswers;
import com.aawhere.know.TrailingSubjectChoiceQuestion;
import com.aawhere.know.TrailingSubjectQuestion;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.number.ByteUtilsExtended;
import com.aawhere.know.Answer;
import com.aawhere.know.Answers;
import com.aawhere.know.Knowledgeable;
import com.aawhere.know.Question;
import com.aawhere.know.ResultsAnswer;
import com.aawhere.know.SimpleAnswer;
import com.aawhere.know.SimpleQuestion;
import com.aawhere.lang.string.StringUtilsExtended;
import com.aawhere.robot.map.ManualNavigator;
import com.aawhere.robot.map.place.NavigationInstructions;
import com.aawhere.robot.map.place.PlaceNavigator;
import com.aawhere.robot.motion.MotionService;
import com.aawhere.robot.motion.Mover;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeRangeSet;
import com.google.common.primitives.Bytes;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

/**
 * A Mover to Move an IRobot Create2 Robot base connected to an Android device by a USB cable.  See
 * https://github.com/mik3y/usb-serial-for-android.
 * <p/>
 * <p/>
 * <p/>
 * Created by aroller on 8/16/15.
 */
public class IRobotCreate2Mover implements Mover {
    public static String TAG = IRobotCreate2Mover.class.getName();
    private static final int STOP_MMPS = 0;
    private static final int MAX_MMPS = 500;
    private static final int MIN_MMPS = -500;
    private static final Range<Integer> RADIUS_RANGE = Range.open(-2000, 2000);
    private static final Range<Integer> MMPS_RANGE = Range.closed(MIN_MMPS, MAX_MMPS);
    /**
     * MMPS speed to go for safe operation.
     */
    public static final int SAFE_MMPS = 100;
    /**
     * Keeps track of the instructions to ensure multiple commands are not issued wasting resources
     * and causing unwanted cumulative behaviors.
     */
    private NavigationInstructions previousInstructions;

    private static final HashMap<IRobotQuestion, HashMap<RangeSet<Integer>, Answer>> answeredQuestions = new HashMap<>();
    private static final Map<String, IRobotQuestion> questions;

    static {
        Answer notCharging = new SimpleAnswer("I'm not charging");
        Answer reconditioning = new SimpleAnswer("I'm charging after being very drained");
        Answer fullCharging = new SimpleAnswer("I am charging");
        Answer trickleCharging = new SimpleAnswer("I am charging, but not very fast");
        Answer waitingToCharge = new SimpleAnswer("I'm waiting to charge");
        Answer chargingFault = new SimpleAnswer("I am unable to charge");
        {
            IRobotQuestion chargingState = new IRobotQuestion("are you charging",
                    SensorPacket.CHARGING_STATE.command(), 1);
            answer(chargingState, notCharging, Range.singleton(0));
            answer(chargingState, reconditioning, Range.singleton(1));
            answer(chargingState, fullCharging, Range.singleton(2));
            answer(chargingState, trickleCharging, Range.singleton(3));
            answer(chargingState, waitingToCharge, Range.singleton(4));
            answer(chargingState, chargingFault, Range.singleton(5));
        }
        {
            IRobotQuestion docked = new IRobotQuestion("are you docked",
                    SensorPacket.CURRENT);
            Answer yes = new SimpleAnswer("Yes, I'm docked");
            Answer no = new SimpleAnswer("No, I'm not docked");
            answer(docked, yes, Range.atLeast(0));
            answer(docked, no, Range.lessThan(0));

        }

        {
            IRobotQuestion dock = new IRobotQuestion("dock", Opcode.DOCK.bytes, 0);
            answer(dock, new SimpleAnswer("Heading to my dock"));
        }
        {
            IRobotQuestion dock = new IRobotQuestion("all", SensorPacket.ALL.command(),
                    SensorPacket.ALL.numberOfBytes);
            answer(dock, new SimpleAnswer("All sensors requested"));
        }
        {
            IRobotQuestion question = new IRobotQuestion("what is your current",
                    SensorPacket.CURRENT);
            Answer answer = new ResultsAnswer("My current is %d mA", question);
            answer(question, answer, Range.<Integer>all());
        }


        questions = Maps.uniqueIndex(answeredQuestions.keySet(), Functions.toStringFunction());


    }

    private static void answer(IRobotQuestion question, Answer answer) {
        answer(question, answer, (Range<Integer>) null);
    }

    private static void answer(IRobotQuestion question, Answer answer, Range<Integer> responseCode) {
        TreeRangeSet<Integer> rangeSet;
        if (responseCode != null) {
            rangeSet = TreeRangeSet.create();
            rangeSet.add(responseCode);
        } else {
            rangeSet = null;
        }
        answer(question, answer, rangeSet);
    }

    private static void answer(IRobotQuestion question, Answer answer, RangeSet<Integer> ranges) {

        HashMap<RangeSet<Integer>, Answer> answerMap = answeredQuestions.get(question);
        if (answerMap == null) {
            answerMap = new HashMap<>();
            answeredQuestions.put(question, answerMap);
        }

        if (ranges == null) {
            //add a single answer since there is no criteria
            answerMap.put(null, answer);
        } else {
            answerMap.put(ranges, answer);
        }
    }

    enum Opcode {

        DRIVE(137),
        SAFE(131),
        /**
         * Ready the robot for commands. Use the Start command (128) to change the mode to Passive.
         */
        START(128),
        RESET(7),
        STOP(173),
        /**
         * Return to charging dock when it is in view.
         */
        DOCK(143),
        /**
         * sends a single request for a sensor state. Used with {@link SensorPacket}.
         */
        SENSORS(142),
        /**
         * This command starts a stream of data packets. The list of packets requested is sent every
         * 15 ms, which is the rate Roomba uses to update data.
         */
        STREAM(148),
        /**
         * This command lets you stop and restart the steam without clearing the list of requested
         * packets.
         */
        STREAM_PAUSE_RESUME(150);

        private final byte value;
        private final byte[] bytes;

        Opcode(int code) {
            this.value = (byte) code;
            this.bytes = new byte[]{value};
        }
    }

    /**
     * Registry by Packet ID populated during enum contruction
     */
    private static final Map<Byte, SensorPacket> SENSOR_PACKETS = sensorPackets();
    private static final Map<String, SensorPacket> SENSOR_PACKET_NAMES = sensorPacketNames();

    private static Map<String, SensorPacket> sensorPacketNames() {

        final ArrayList<SensorPacket> sensorPackets = Lists.newArrayList(SensorPacket.values());
        return Maps.uniqueIndex(sensorPackets, Functions.toStringFunction());
    }

    private static Map<Byte, SensorPacket> sensorPackets() {
        HashMap<Byte, SensorPacket> map = Maps.newHashMap();
        //sensor packet init
        final SensorPacket[] sensorPackets = SensorPacket.values();
        for (int i = 0; i < sensorPackets.length; i++) {
            SensorPacket sensorPacket = sensorPackets[i];
            map.put(sensorPacket.packetId, sensorPacket);
        }

        return map;
    }

    /**
     * Roomba sends back one of 58 different sensor data packets, depending on the value of the
     * packet data byte, when responding to a Sensors command, Query List command, or Stream
     * command’s request for a packet of sensor data bytes. Some packets contain groups of other
     * packets. Some of the sensor data values are 16 bit values. Most of the packets (numbers 7 –
     * 58) contain the value of a single sensor or variable, which can be either 1 byte or 2 bytes.
     * Two byte packets correspond to 16-bit values, sent high byte first. Some of the packets (0-6,
     * 100-107) contain groups of the single-value packets.
     */
    enum SensorPacket {

        /**
         * This code indicates Roomba’s current charging state.
         */
        CHARGING_STATE(21, 1),
        CURRENT(23, 2),
        /**
         * The state of the bumper (0 = no bump, 1 = bump) and wheel drop sensors (0 = wheel raised,
         * 1 = wheel dropped) are sent as individual bits.
         */
        BUMPS_WHEEL_DROPS(7, 1),
        ALL(6, 52);

        /**
         * The id that communicates the sensor of interest.
         */
        public final byte packetId;

        /**
         * The number of bytes returned
         */
        public final int numberOfBytes;

        private SensorPacket(int packetId, int numberOfBytes) {
            this.packetId = (byte) packetId;
            this.numberOfBytes = numberOfBytes;
        }

        public byte[] command() {
            return new byte[]{Opcode.SENSORS.value, packetId};
        }

        /**
         * Depending of #numberOfBytes this will read the correct number of bytes and return an
         * integer.
         *
         * @param iterator
         * @return the int reprsenting the sensor reading
         * @throws IndexOutOfBoundsException if the iterator does not supply the expected
         * @throws IllegalArgumentException  if the number of bytes of this is not yet handled
         */
        public int read(Iterator<Byte> iterator) {
            if (numberOfBytes == 1) {
                return iterator.next();
            } else if (numberOfBytes == 2) {
                return ByteUtilsExtended.highLowShort(new byte[]{iterator.next(), iterator.next()});
            } else {
                throw new IllegalArgumentException(numberOfBytes + " not supported");
            }
        }
    }


    /**
     * This code indicates Roomba’s current charging state. The code = Enum#ordinal() so enumeration
     * order must be maintained.
     */
    enum ChargingState {

        NOT_CHARGING, RECONDITIONING_CHARGING, FULL_CHARGING, TRICKLE_CHARGING, WAITING, CHARGING_FAULT_CONDITION;
    }

    enum Command {
        STREAM_PAUSE(Opcode.STREAM_PAUSE_RESUME, 0), STREAM_RESUME(Opcode.STREAM_PAUSE_RESUME, 1);


        public final byte[] bytes;

        Command(Opcode opcode, int data) {
            this(new byte[]{opcode.value, (byte) data});
        }

        Command(byte[] bytes) {
            this.bytes = bytes;
        }
    }


    enum DriveCommand {

        /**
         * Straight forward.
         */
        STRAIGHT(0x8000),
        /**
         * Sends a drive command with a zero radius.  speed must be zero for this to work
         */
        STOP(0),
        /**
         * Spin Right (clockwise)
         */
        RIGHT(-1),
        /**
         * Spin left (counterclockwise)
         */
        LEFT(1);
        public final int radius;

        DriveCommand(int radius) {
            this.radius = radius;
        }

        /**
         * @param mmpsSpeed
         * @return
         */
        public byte[] bytes(int mmpsSpeed) {
            //the smaller the radius...the sharper the turn

            int adjustedRadius = radius;
            if ((this == RIGHT || this == LEFT) && Math.abs(mmpsSpeed) > SAFE_MMPS) {
                //the faster the speed the less sharp the turn
                //zero speed
                adjustedRadius = radius * mmpsSpeed;
            }
            return Bytes.concat(Opcode.DRIVE.bytes, highLowBytes(mmpsSpeed),
                    highLowBytes(adjustedRadius));
        }
    }

    private String description;
    /**
     * The USB port for all communications.
     */
    private UsbSerialPort port;

    private int writeTimeoutMillis = 100;

    /**
     * Used to avoid repeating commands
     */
    private byte[] previousCommand;

    /**
     * The drive speed used for navigating the robot.  It may be increased or decreased through
     * instructions.
     */
    private int currentSpeed = STOP_MMPS;

    private NavigationInstructions.Speed speedTarget;

    /**
     * Used to construct all instances of IRobotCreate2Mover.
     */
    public static class IRobotCreate2MoverBuilder extends ObjectBuilder<IRobotCreate2Mover> {

        private UsbManager manager;

        private IRobotCreate2MoverBuilder() {
            super(new IRobotCreate2Mover());
        }

        public IRobotCreate2MoverBuilder manager(UsbManager manager) {
            this.manager = manager;
            return this;
        }

        @Override
        protected void validate() {
            super.validate();
            Assertion.assertNotNull("manager", manager);
        }

        public IRobotCreate2Mover build() {
            IRobotCreate2Mover built = super.build();

            List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().findAllDrivers(
                    manager);
            if (availableDrivers.size() > 0) {
                final UsbSerialDriver usbSerialDriver = availableDrivers.get(0);
                built.description = "robot Connected";


                UsbDeviceConnection connection = manager.openDevice(usbSerialDriver.getDevice());
                if (connection == null) {
                    built.description = "Unable to communicate with connected Robot.";
                } else {

                    //making a big assumption that one and only one port is available
                    built.port = usbSerialDriver.getPorts().get(0);

                    try {
                        //wake up the robot
                        //http://robotics.stackexchange.com/questions/7895/irobot-create-2-powering-up-after-sleep/7900#7900

                        built.port.open(connection);
                        built.port.setParameters(115200, 8, UsbSerialPort.STOPBITS_1,
                                UsbSerialPort.PARITY_NONE);

                        built.command(Opcode.START);

                        built.command(Opcode.SAFE);
                        built.move(NavigationInstructions.stop());

                    } catch (IOException e) {
                        built.description = "Initialization failed " + e.getMessage();
                        try {
                            built.port.close();
                            built.port = null;
                        } catch (IOException e1) {

                        }
                    }
                }

            } else {
                built.description = "Since I'm not connected to the robot, I'll have to use you as my wheels.";
            }
            return built;
        }

    }//end Builder

    public static IRobotCreate2MoverBuilder builder() {
        return new IRobotCreate2MoverBuilder();
    }

    /**
     * Use {@link IRobotCreate2MoverBuilder} to construct IRobotCreate2Mover
     */
    private IRobotCreate2Mover() {
    }


    @Override
    public Result move(NavigationInstructions navigationInstructions) {
        Result result;
        //avoid sending repeat requests if the same
        if (navigationInstructions != null && (previousInstructions == null || !previousInstructions.equals(
                navigationInstructions))) {
            result = moveForced(navigationInstructions);

        } else {
            //it's just a repeat so consider it successful
            result = Result.SUCCESS;
        }
        return result;
    }

    /**
     * bypasses repeat checks and forces the move.
     */
    private Result moveForced(NavigationInstructions navigationInstructions) {
        Result result;//fast exit if initialization failed
        if (isReady()) {
            DriveCommand command;
            Opcode followUpCommand = null;
            //adjust the speed if instructed to do so
            if (navigationInstructions.speed() != null) {
                speedTarget = navigationInstructions.speed();
            } else if (previousInstructions != null && previousInstructions.instruction() != null &&
                    (!previousInstructions.instruction().equals(
                            navigationInstructions.instruction())
                            //race conditions can make the target speed null when should be something else
                            || navigationInstructions.instruction() != PlaceNavigator.Instruction.STOP && speedTarget == null)) {
                //if there is a change in instruction then always revert to slow
                speedTarget = NavigationInstructions.Speed.SLOW;
            }

            if (navigationInstructions.instruction() != null) {
                switch (navigationInstructions.instruction()) {
                    case RIGHT:
                        command = DriveCommand.RIGHT;
                        break;
                    case LEFT:
                        command = DriveCommand.LEFT;
                        break;
                    case FORWARD:
                    case REVERSE:
                        //reverse is a negative speed issue and is adjusted by currentSpeed using #speed()
                        command = DriveCommand.STRAIGHT;
                        break;
                    case STOP:
                    default:
                        command = DriveCommand.STOP;

                }
            } else {
                command = DriveCommand.STOP;
            }


            if (command == DriveCommand.STOP) {
                //current speed is adjusted by timer
                stop();
            }

            int speed = this.currentSpeed;

            //hard stop if going slow enough
            if (mmps(speedTarget) == STOP_MMPS && currentSpeed != STOP_MMPS) {
                if (Math.abs(currentSpeed) <= SAFE_MMPS) {
                    speed = STOP_MMPS;
                    //allow polling to modify current speed.
                }
            }

            //sending safe again does not harm if already there, but ensures it will be ready to drive.
            command(Opcode.SAFE);
            result = command(command.bytes(speed));
            if (followUpCommand != null) {
                command(followUpCommand);
            }

        } else {
            result = Result.FAILURE;
        }
        this.previousInstructions = navigationInstructions;
        return result;
    }

    private Result command(Command command) {
        final byte[] bytes = command.bytes;
        return command(bytes);
    }


    private Result command(Opcode opcode) {
        return command(opcode.bytes);
    }

    private Result command(byte[] bytes) {
        try {
            port.write(bytes, writeTimeoutMillis);
            return Result.SUCCESS;
        } catch (IOException e) {
            return Result.FAILURE;
        }
    }


    /**
     * indicates if this mover is able to command and ready to receive navigation instructions.
     */
    public boolean isReady() {
        return port != null;
    }

    @Override
    public String describe() {
        return description;
    }


    private static byte[] highLowBytes(int value) {
        return ByteUtilsExtended.highLowBytes((short) value);
    }

    /**
     * However this object goes away we should ensure the stop command is sent to the IRobot if
     * possible.
     *
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        try {
            if (isReady()) {
                command(Opcode.STOP);
                command(Command.STREAM_PAUSE);
            }
        } catch (Exception e) {
            //ignore since it can't be cleaned up
        }

    }

    /**
     * reads the bytes synchronously blocking until all bytes are read or #writeTimeoutMillis has
     * been reached
     *
     * @return
     * @throws IOException
     */
    protected byte[] blockingRead() throws IOException {
        byte[] bytes = new byte[1024];
        long start = System.currentTimeMillis();
        boolean timedOut;

        int numberOfBytesRead;
        do {
            numberOfBytesRead = port.read(bytes,
                    writeTimeoutMillis);
            timedOut = (System.currentTimeMillis() - start) > writeTimeoutMillis;
        } while (numberOfBytesRead <= 0 && !timedOut);

        return bytes;
    }


    private ResultsAnswer commandRequest(final PlaceNavigator.Instruction instruction, String message, KnownAnswers.KnownAnswersBuilder answers, final MotionService motionService) {
        final String commandName = instruction.name().toLowerCase();
        SimpleQuestion question = new SimpleQuestion(commandName);
        ResultsAnswer answer = ResultsAnswer.builder(message).supplier(
                new Supplier<Object[]>() {
                    @Override
                    public Object[] get() {
                        //TODO:told by whom?
                        final ManualNavigator navigator = ManualNavigator.builder().instructions(
                                NavigationInstructions.builder().instruction(
                                        instruction).reason(
                                        "I was told to " + instruction).build()).build();
                        motionService.navigator(navigator);
                        return new Object[]{};
                    }
                }).question(question).build();

        answers.answer(question, answer);
        return answer;
    }

    /**
     * Commands specifically related to moving the irobot.
     *
     * @return
     */
    public KnownAnswers knowledgeAboutMoving(final MotionService motionService) {


        final KnownAnswers.KnownAnswersBuilder knownAnswersBuilder = KnownAnswers.builder();

        {
            SimpleQuestion resetQuestion = new SimpleQuestion("reset your robot");
            ResultsAnswer resetAnswer = ResultsAnswer.builder(
                    "the reset returned %s").question(
                    resetQuestion).supplier(
                    new Supplier<Object[]>() {
                        @Override
                        public Object[] get() {
                            command(Opcode.RESET);
                            Object[] result;
                            try {
                                byte[] bytes = blockingRead();
                                return new Object[]{new String(bytes)};
                            } catch (IOException e) {
                                return new Object[]{new SimpleAnswer(
                                        "Failed to read the reset results because " + e.getMessage())};
                            }
                        }
                    }).build();
            knownAnswersBuilder.answer(resetQuestion, resetAnswer);
        }
        //sensor question
        {
            final TrailingSubjectQuestion sensorQuestion = new TrailingSubjectQuestion(
                    "what is the robot sensor reading for ");
            Function<Question, Object[]> sensorFunction = new Function<Question, Object[]>() {
                @Nullable
                @Override
                public Object[] apply(Question input) {
                    Object[] result;
                    final String sensorName = sensorQuestion.subject(input);
                    if (!StringUtilsExtended.isEmpty(sensorName)) {
                        final String bestMatch = StringUtilsExtended.bestMatch(sensorName,
                                SENSOR_PACKET_NAMES.keySet());
                        if (bestMatch != null) {
                            final SensorPacket sensorPacket = SENSOR_PACKET_NAMES.get(
                                    bestMatch);
                            try {
                                final Integer value = read(sensorPacket);
                                if (value != null) {
                                    result = new Object[]{sensorName, value};
                                } else {
                                    result = ResultsAnswer.results(
                                            "the robot isn't cooperating");
                                }
                            } catch (IOException e) {
                                result = ResultsAnswer.results(
                                        "I had a problem talking to the robot " + e.getMessage());
                            }
                        } else {
                            result = ResultsAnswer.results(
                                    "I can't find a sensor similar to " + sensorName);
                        }

                    } else {
                        result = ResultsAnswer.results(
                                "please provide the name of the sensor you seek");
                    }
                    return result;
                }
            };
            knownAnswersBuilder.answer(sensorQuestion,
                    ResultsAnswer.builder("%s is %s").question(sensorQuestion).function(
                            sensorFunction).build());
        }

        {
            final Iterable<String> choices = Iterables.transform(
                    Iterables.transform(ImmutableList.copyOf(NavigationInstructions.Speed.values()),
                            Functions.toStringFunction()), StringUtilsExtended.lowerCaseFunction());
            final TrailingSubjectChoiceQuestion question = TrailingSubjectChoiceQuestion.builderWithChoices(
                    "change your speed to ").subject("rate").choices(choices).build();
            Function<Question, Object[]> function = new Function<Question, Object[]>() {
                @Nullable
                @Override
                public Object[] apply(@Nullable Question input) {
                    Object[] results;
                    final String subject = question.subject(input);
                    if (subject != null) {
                        try {
                            final NavigationInstructions.Speed speed = NavigationInstructions.Speed.valueOf(
                                    StringUtilsExtended.toConstantCase(subject));

                            speedTarget = speed;
                            results = ResultsAnswer.results(subject);

                        } catch (Exception e) {
                            results = ResultsAnswer.alternateAnswer(
                                    subject + " is not a speed. choose from " + StringUtils.join(
                                            choices));
                        }

                    } else {
                        results = ResultsAnswer.alternateAnswer(
                                "provide one of " + StringUtils.join(
                                        choices));
                    }
                    return results;
                }
            };
            ResultsAnswer answer = ResultsAnswer.builder("changing speed to %s").question(
                    question).function(function).build();
            knownAnswersBuilder.answer(question, answer);

        }

        //what is your speed
        {
            SimpleQuestion question = new SimpleQuestion("what is your speed");
            Supplier supplier = new Supplier() {
                @Override
                public Object get() {
                    String answer;
                    answer = speedString();
                    return ResultsAnswer.results(answer);
                }


            };
            knownAnswersBuilder.answer(question,
                    ResultsAnswer.builder("my speed is %s").question(question).supplier(
                            supplier).build());
        }

        //faster
        {
            SimpleQuestion question = new SimpleQuestion("faster");
            knownAnswersBuilder.answer(question,
                    ResultsAnswer.builder("my speed is %s").question(question).supplier(
                            new Supplier<Object[]>() {
                                @Override
                                public Object[] get() {
                                    final int ordinal = Math.min(
                                            NavigationInstructions.Speed.FAST.ordinal(),
                                            speedTarget.ordinal() + 1);
                                    speedTarget = NavigationInstructions.Speed.values()[ordinal];
                                    return new Object[]{speedString()};
                                }
                            }).build());
        }
        //slower
        {
            SimpleQuestion question = new SimpleQuestion("slower");
            knownAnswersBuilder.answer(question,
                    ResultsAnswer.builder("my speed is %s").question(question).supplier(
                            new Supplier<Object[]>() {
                                @Override
                                public Object[] get() {
                                    if (speedTarget == null || speedTarget == NavigationInstructions.Speed.SLOW) {
                                        stop();
                                    } else {
                                        speedTarget = NavigationInstructions.Speed.values()[speedTarget.ordinal() - 1];
                                    }
                                    return new Object[]{speedString()};
                                }
                            }).build());
        }
        //simple commands
        {
            commandRequest(PlaceNavigator.Instruction.STOP, "stopped", knownAnswersBuilder,
                    motionService);
            commandRequest(PlaceNavigator.Instruction.LEFT, "rotating left",
                    knownAnswersBuilder, motionService);
            commandRequest(PlaceNavigator.Instruction.RIGHT, "rotating right",
                    knownAnswersBuilder, motionService);
            commandRequest(PlaceNavigator.Instruction.FORWARD, "going forward",
                    knownAnswersBuilder, motionService);
            commandRequest(PlaceNavigator.Instruction.REVERSE, "backing up",
                    knownAnswersBuilder, motionService);
        }

        //start polling for safety
        pollSensors(motionService);

        return knownAnswersBuilder.subject("move the robot").build();

    }

    private String speedString() {
        String answer;
        if (speedTarget == null) {
            answer = "stopped";
        } else {
            answer = speedTarget.toString();
        }
        return answer;
    }

    /**
     * Commands related to querying the irobot for sense.
     *
     * @return
     */
    public Knowledgeable knowledgeable() {
        return new Knowledgeable() {


            @Override
            public ListenableFuture<Answer> answer(Question question, KnowledgeBase knowledgeBase) {
                final Set<IRobotQuestion> iRobotQuestions = answeredQuestions.keySet();
                //TODO: look for close matches
                final String bestMatch = questions.keySet().contains(
                        question.toString()) ? question.toString() : null;
                Answer answer;
                if (bestMatch != null) {
                    final IRobotQuestion iRobotQuestion = questions.get(bestMatch);
                    final Result writeResult = command(iRobotQuestion.writeBytes());
                    if (writeResult == Result.SUCCESS) {
                        final HashMap<RangeSet<Integer>, Answer> answers = answeredQuestions.get(
                                iRobotQuestion);
                        if (iRobotQuestion.numberOfReadBytes() == 0) {
                            //the answer is a command without a read
                            answer = answers.values().iterator().next();
                        } else {
                            byte[] readBytes = new byte[256];
                            try {
                                int numberOfBytesRead;

                                long start = System.currentTimeMillis();
                                boolean timedOut;
                                //FIXME:call blockingRead
                                do {
                                    numberOfBytesRead = port.read(readBytes,
                                            writeTimeoutMillis);
                                    timedOut = (System.currentTimeMillis() - start) > writeTimeoutMillis;
                                } while (numberOfBytesRead <= 0 && !timedOut);


                                if (numberOfBytesRead == iRobotQuestion.numberOfReadBytes()) {
                                    int result;
                                    if (iRobotQuestion.numberOfReadBytes() <= 2) {
                                        if (iRobotQuestion.numberOfReadBytes() == 2) {
                                            result = ByteUtilsExtended.highLowShort(readBytes);
                                        } else {
                                            result = readBytes[0];
                                        }
                                        final Iterator<RangeSet<Integer>> rangeSets = answers.keySet().iterator();
                                        Answer resultAnswer = null;
                                        while (rangeSets.hasNext() && resultAnswer == null) {
                                            RangeSet<Integer> rangeSet = rangeSets.next();
                                            if (rangeSet.contains(result)) {
                                                resultAnswer = answers.get(rangeSet);
                                            }
                                        }
                                        if (resultAnswer != null) {
                                            if (resultAnswer instanceof ResultsAnswer) {
                                                answer = ((ResultsAnswer) resultAnswer).answer(
                                                        result);
                                            } else {
                                                answer = resultAnswer;
                                            }

                                        } else {
                                            answer = new SimpleAnswer(
                                                    result + " has no corresponding answer");
                                        }
                                    } else {
                                        answer = new SimpleAnswer(
                                                "I don't know how to read " + iRobotQuestion.numberOfReadBytes() + " bytes");

                                    }


                                } else {
                                    if (timedOut) {
                                        answer = new SimpleAnswer(
                                                "The robot didn't respond in time");
                                    } else {
                                        answer = new SimpleAnswer(
                                                "Expecting " + iRobotQuestion.numberOfReadBytes() + " bytes, but received " + numberOfBytesRead);
                                    }
                                }

                            } catch (IOException e) {
                                answer = new SimpleAnswer(e.getMessage());
                            }
                        }
                    } else {
                        answer = new SimpleAnswer("Failed requesting the status " + description);
                    }

                    final SettableFuture<Answer> future = SettableFuture.create();
                    future.set(answer);
                    return future;
                } else {
                    return Answers.future(Answers.unknown());
                }


            }

            @Override
            public boolean canAnswer(Question question) {
                return containsQuestionKey(question);
            }

            private boolean containsQuestionKey(Question question) {
                return questions.keySet().contains(
                        question.toString().toLowerCase());
            }

            @Override
            public Double answerConfidence(Question question) {
                return containsQuestionKey(
                        question) ? Knowledgeable.ABSOLUTELY_CONFIDENT : Knowledgeable.NO_CONFIDENCE;
            }

            @Override
            public String subject() {
                return "check robot sensors";
            }
        };
    }


    /**
     * @param sensorPacket
     * @return the value read or null if the command failed
     * @throws IOException
     */
    public Integer read(SensorPacket sensorPacket) throws IOException {
        final Result result = command(sensorPacket.command());
        if (result == Result.SUCCESS) {
            final byte[] bytes = blockingRead();
            return sensorPacket.read(Bytes.asList(bytes).iterator());
        } else {
            return null;
        }
    }

    private void stop() {
        this.speedTarget = null;
    }

    /**
     * returns the  mm per second that relates to the speed for the previous instruction (reverse).
     *
     * @param speed
     * @return
     */
    private int mmps(NavigationInstructions.Speed speed) {
        int result;
        if (speed == null) {
            result = STOP_MMPS;
        } else {

            switch (speed) {
                case FAST:
                    result = MAX_MMPS;
                    break;
                case SLOW:
                    result = SAFE_MMPS;
                    break;
                case MEDIUM:
                    result = MAX_MMPS / 2;
                    break;
                default:
                    throw new IllegalArgumentException(speed.toString());
            }
        }
        if (previousInstructions != null && previousInstructions.instruction() == PlaceNavigator.Instruction.REVERSE) {
            result *= -1;
        }
        return result;
    }

    private void pollSensors(final MotionService motionService) {
        final long UPDATE_INTERVAL_MS = writeTimeoutMillis;

        //request a repeating stream of sensor data
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true && !Thread.currentThread().isInterrupted()) {
                    try {
                        //ensure polling happens no more than the iRobot will update
                        Thread.sleep(UPDATE_INTERVAL_MS);
                        try {
                            //only read while moving forward.  Others shouldn't result in bumping
                            if (previousInstructions != null && previousInstructions.instruction() == PlaceNavigator.Instruction.FORWARD) {
                                final Integer value = read(SensorPacket.BUMPS_WHEEL_DROPS);
                                if (value == null || value != 0) {
                                    Log.w(TAG, "Bump detected...requesting stop: " + value);

                                    motionService.navigator(ManualNavigator.stop(
                                            "I stopped because the iRobot bumped into something"));
                                }
                            }

                        } catch (IOException e) {
                            Log.e(TAG, "problem during update ", e);
                        }

                        if (previousInstructions != null) {
                            //adjust speed incrementally to avoid sharp accelerations
                            //10 mmps adjustment, 10x per second = 100mmps acceleration.  Stop to full speed in 5 seconds
                            //20 mmps adjustment, 10x per second = 200mmps acceleration.  Stop to full speed in 2.5 seconds
                            int increment = 25;
                            final int targetMmps = mmps(speedTarget);
                            boolean move = false;
                            String reason = null;

                            //increase/reduce incrementally until target speed is reached
                            if (currentSpeed > targetMmps) {
                                currentSpeed -= increment;
                                currentSpeed = Math.max(targetMmps, currentSpeed);
                                move = true;
                                reason = "slowing down to " + speedTarget + " speed";
                            } else if (currentSpeed < targetMmps) {
                                currentSpeed += increment;
                                currentSpeed = Math.min(targetMmps, currentSpeed);
                                move = true;
                                reason = "speeding up to " + speedTarget + " speed";
                            }
                            //repeat the command since the speed is continuing to adjust
                            if (move) {
                                //we want this to be forced
                                final NavigationInstructions navigationInstructions = NavigationInstructions.clone(
                                        previousInstructions).reason(reason).build();
                                moveForced(navigationInstructions);
                                Log.v(TAG, targetMmps + " is not " + currentSpeed);
                            }
                        }
                    } catch (InterruptedException e) {
                        Log.e(TAG, "problem during poll", e);
                    }
                }
            }
        }).start();

    }


}
