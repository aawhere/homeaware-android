package com.aawhere.robot.irobot;

import com.aawhere.know.SimpleQuestion;

/**
 * Created by aroller on 8/31/15.
 */
public class IRobotQuestion extends SimpleQuestion {

    private byte[] writeBytes;
    private final int numberOfReadBytes;

    public IRobotQuestion(String question, byte[] writeBytes) {
        this(question, writeBytes, 0);
    }

    public IRobotQuestion(String question, byte[] writeBytes, int numberOfReadBytes) {
        super(question);
        this.writeBytes = writeBytes;
        this.numberOfReadBytes = numberOfReadBytes;
    }

    public IRobotQuestion(String question, IRobotCreate2Mover.SensorPacket sensorPacket) {
        this(question, sensorPacket.command(), sensorPacket.numberOfBytes);
    }

    public int numberOfReadBytes() {
        return numberOfReadBytes;
    }

    public byte[] writeBytes() {
        return writeBytes;
    }
}
