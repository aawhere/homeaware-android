package com.aawhere.robot.comm;

import android.content.Context;
import android.util.Log;

import com.aawhere.know.KnowledgeBase;
import com.aawhere.know.Knowledgeable;
import com.aawhere.know.KnowledgeablesJSpeechGrammarFormat;
import com.aawhere.know.KnownAnswers;
import com.aawhere.know.Question;
import com.aawhere.know.ResultsAnswer;
import com.aawhere.know.SimpleQuestion;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

/**
 * An offline speech recognizer used to recognize known questions from the KnowledgeBase.
 * <p/>
 * <p/>
 * The input listens constantly for the given activation phrase Created by aroller on 9/16/15.
 */
public class SphinxSpeechInput implements RecognitionListener {

    public static final String TAG = SphinxSpeechInput.class.getName();

    private static final String ACTIVATION_KEY = "wakeup";
    private static final String KNOWLEDGE_KEY = "know";

    private KnowledgeBase knowledgeBase;
    private SpeechRecognizer recognizer;
    private String activationPhrase;
    private MessageReceivedListener listener;
    private KnowledgeablesJSpeechGrammarFormat grammar;
    private boolean listening = false;

    /**
     * Since this is a service, it must be able to communicate with the system and not rely on
     * others being able to catch problems.
     */
    private Communicator communicator;


    /**
     * Used to construct all instances of SphinxSpeechInput.
     */
    public static class SphinxSpeechInputBuilder extends ObjectBuilder<SphinxSpeechInput> {
        private Context context;

        private SphinxSpeechInputBuilder() {
            super(new SphinxSpeechInput());
        }

        public SphinxSpeechInputBuilder context(Context context) {
            this.context = context;
            return this;
        }

        public SphinxSpeechInputBuilder communicator(Communicator communicator) {
            building.communicator = communicator;
            return this;
        }

        public SphinxSpeechInputBuilder activationPhrase(String activationPhrase) {
            building.activationPhrase = activationPhrase;
            return this;
        }

        public SphinxSpeechInputBuilder knowledgeBase(KnowledgeBase knowledgeBase) {
            building.knowledgeBase = knowledgeBase;
            return this;
        }

        public SphinxSpeechInputBuilder listener(MessageReceivedListener listener) {
            building.listener = listener;
            return this;
        }

        @Override
        protected void validate() {
            super.validate();
            Assertion.assertNotNull("listener", building.listener);
            Assertion.assertNotNull("knowledgeBase", building.knowledgeBase);
            Assertion.assertNotNull("activationPhrase", building.activationPhrase);
            Assertion.assertNotNull("context", this.context);
            Assertion.assertNotNull("communicator", building.communicator);
        }

        public SphinxSpeechInput build() {
            try {
                return buildVerbose();
            } catch (IOException e) {
                //it's encourage to use #setup so the exception can be caught
                throw new RuntimeException(e);
            }
        }

        /**
         * builds and reports the exception.  This should be done in a task since setup can take
         * some time.
         *
         * @return
         * @throws IOException
         */
        public SphinxSpeechInput buildVerbose() throws IOException {
            SphinxSpeechInput built = super.build();

            Assets assets = new Assets(context);
            File assetsDir = assets.syncAssets();
            built.recognizer = SpeechRecognizerSetup.defaultSetup()
                    .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                    .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))
                            // To disable logging of raw audio comment out this call (takes a lot of space on the device)
                    .setRawLogDir(assetsDir)
                            // Threshold to tune for keyphrase to balance between false alarms and misses
                    .setKeywordThreshold(1e-45f)
                            // Use context-independent phonetic search, context-dependent is too slow for mobile
                    .setBoolean("-allphone_ci", true)
                    .getRecognizer();
            built.recognizer.addListener(built);
            built.recognizer.addKeyphraseSearch(ACTIVATION_KEY, built.activationPhrase);


            built.listenForKnowledge();
            return built;
        }

    }//end Builder

    public static SphinxSpeechInputBuilder builder() {
        return new SphinxSpeechInputBuilder();
    }

    private void listenForActivation() {
        stopListening();
        this.recognizer.startListening(ACTIVATION_KEY);
        Log.i(TAG, "listening for activation");
    }

    /**
     * Listens to all words attempting to match to the knowledge grammar. The grammar will be
     * rebuilt if needed to reflect new knowledge.
     * <p/>
     * Made available to allow control by the speaker to avoid listening to itself.
     */
    public void listenForKnowledge() {

        //it doesn't hurt to stop it again just in case
        stopListening();
        //we must build the grammar if the knowledge base has changed

        if (listening) {
            knowledgeBaseGrammar();

            recognizer.startListening(KNOWLEDGE_KEY);
            Log.i(TAG, "listening knowledge for ");
        }
    }

    /**
     * made available so a speaker can manage when listening happens
     */
    public void stopListening() {
        this.recognizer.stop();
    }

    /**
     * Use {@link SphinxSpeechInputBuilder} to construct SphinxSpeechInput
     */
    private SphinxSpeechInput() {
    }


    @Override
    public void onBeginningOfSpeech() {
        Log.i(TAG, "beginning of speech " + recognizer.getSearchName());
    }

    @Override
    public void onEndOfSpeech() {
        Log.i(TAG, "end of speech " + recognizer.getSearchName());
        //send the word to results
        if (!recognizer.getSearchName().equals(ACTIVATION_KEY)) {
            recognizer.stop();
        }
    }

    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis != null) {
            final String text = hypothesis.getHypstr();
            Log.i(TAG, "partial " + text);

            if (text.equals(activationPhrase)) {
                //listenForActivation for commands until a timeout of de-activation command is given
                //TODO:add a deactivation question/answer to the knowledge base
                listenForKnowledge();
            }
        }
    }


    @Override
    public void onResult(Hypothesis hypothesis) {
        if (hypothesis != null) {
            String text = hypothesis.getHypstr();
            Log.i(TAG, "final result " + hypothesis.getBestScore() + " for " + text);

            if (!text.equals(activationPhrase)) {

                boolean activationPhraseIncluded = false;
                //modify text to remove activation phrase
                if (text.startsWith(activationPhrase)) {
                    text = text.replace(activationPhrase, StringUtils.EMPTY).trim();
                    activationPhraseIncluded = true;
                }

                Question question = new SimpleQuestion(text);

                if (knowledgeBase.isConfident(question)) {

                    if (activationPhraseIncluded || scoreQualifies(hypothesis)) {
                        listener.messageReceived(text);
                        //do not listen until answer is spoken otherwise it listens to itself
                    } else {
                        listenForKnowledge();
                    }
                } else {
                    Log.w(TAG, "not confident to speak " + question);
                    listenForKnowledge();
                }

            } else {
                Log.w(TAG, "activation phrase made it to final result");
            }

        } else {
            //hypothesis is null whenver there is a noise.
            if (recognizer.getSearchName().equals(KNOWLEDGE_KEY)) {
                //cancel the request or an infinite loop will occur
                this.recognizer.cancel();
                listenForKnowledge();
            }//else activation shouldn't even be stopped
        }


    }

    private boolean scoreQualifies(Hypothesis hypothesis) {

        final int bestScore = hypothesis.getBestScore();
        final String text = hypothesis.getHypstr();
        final int numberOfWords = StringUtils.countMatches(text, StringUtils.SPACE) + 1;
        final int goodScore = -2000;
        final int adjustedGoodScore = goodScore + ((int) (goodScore * numberOfWords * .33));
        final boolean qualifies = bestScore > adjustedGoodScore;
        if (!qualifies) {
            Log.w(TAG, text + " ignored since score " + bestScore + " < " + adjustedGoodScore);
        }
        return qualifies;
    }

    @Override
    public void onError(Exception e) {
        Log.i(TAG, "error ", e);
    }

    @Override
    public void onTimeout() {
        Log.i(TAG, "timeout");
        //listenForActivation again, but only for activation
        listenForActivation();
    }

    /**
     * Call this when no more calls will be made.
     */
    public void shutDown() {
        this.recognizer.shutdown();
        this.recognizer = null;
    }

    /**
     * Provides status of messages and state of the speech input
     */
    public static interface MessageReceivedListener {
        /**
         * provides the message that has enough confidence
         */
        public void messageReceived(String message);

        /**
         * indicates listening has stopped.
         */
        public void listeningStopped();
    }

    public String knowledgeBaseGrammar() {
        try {

            if (grammar == null || grammar.knowledgeableCountGiven() != knowledgeBase.numberOfKnowledgeables()) {
                this.grammar = KnowledgeablesJSpeechGrammarFormat.builder().activationPhrases(
                        ImmutableSet.of(activationPhrase)).knowledgeables(
                        knowledgeBase.knowledgeables()).build();

                Log.i(TAG, grammar.toString());
                recognizer.getDecoder().setJsgfString(KNOWLEDGE_KEY, grammar.toString());
            }
        } catch (Exception e) {
            communicator.error(getClass(),
                    "I had difficulties learning how to speak: " + e.getMessage(), e);
        }

        return grammar.toString();
    }


    public Knowledgeable knowledgeable() {
        KnownAnswers.KnownAnswersBuilder answers = KnownAnswers.builder().subject(
                "how to listen");
        {
            SimpleQuestion question = new SimpleQuestion("listen");
            ResultsAnswer answer = ResultsAnswer.builder("i'm listening").question(
                    question).supplier(
                    new Supplier<Object[]>() {
                        @Override
                        public Object[] get() {
                            listening = true;
                            listenForKnowledge();
                            return new Object[0];
                        }
                    }).build();
            answers.answer(question, answer);
        }
        //stop listening and go into activate mode
        {
            SimpleQuestion question = new SimpleQuestion("stop listening");
            ResultsAnswer answer = ResultsAnswer.builder(
                    "I'm only listening for " + activationPhrase).question(
                    question).supplier(
                    new Supplier<Object[]>() {
                        @Override
                        public Object[] get() {
                            listening = false;
                            stopListening();
                            return new Object[0];
                        }
                    }).build();
            answers.answer(question, answer);
        }
        return answers.build();
    }
}
