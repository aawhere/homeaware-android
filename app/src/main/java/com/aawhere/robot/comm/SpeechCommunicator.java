package com.aawhere.robot.comm;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import com.aawhere.collections.RangeExtended;
import com.aawhere.know.Knowledgeable;
import com.aawhere.know.KnownAnswers;
import com.aawhere.know.Question;
import com.aawhere.know.ResultsAnswer;
import com.aawhere.know.SimpleQuestion;
import com.aawhere.know.TrailingSubjectQuestion;
import com.aawhere.lang.ObjectBuilder;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Range;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Locale;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Communicates using Android Text to Speech. A single statement may be spoken or this may go into
 * conversation mode where all responses will be spoken.
 * <p/>
 * Created by aroller on 9/15/15.
 */
public class SpeechCommunicator extends Communicator {

    private static final String TAG = SpeechCommunicator.class.getName();

    private static final Range<Double> VOLUME_RANGE = Range.closed(0.0, 1.0);

    /**
     * Used to construct all instances of SpeechCommunicator.
     */
    public static class SpeechCommunicatorBuilder extends ObjectBuilder<SpeechCommunicator> {

        private Context context;
        private UtteranceProgressListener listener;

        private SpeechCommunicatorBuilder() {
            super(new SpeechCommunicator());
        }


        public SpeechCommunicatorBuilder context(Context context) {
            this.context = context;
            return this;
        }

        public SpeechCommunicatorBuilder listener(UtteranceProgressListener utteranceProgressListener) {
            this.listener = utteranceProgressListener;
            return this;
        }

        public SpeechCommunicator build() {
            final SpeechCommunicator built = super.build();
            built.speech = new TextToSpeech(context,
                    new TextToSpeech.OnInitListener() {

                        @Override
                        public void onInit(int status) {
                            if (status == TextToSpeech.SUCCESS) {
                                Log.v(TAG,
                                        "initializing speaker with status " + status + " and listener " + listener);
                                built.speech.setLanguage(Locale.US);
                                if (listener != null) {
                                    built.speech.setOnUtteranceProgressListener(listener);
                                }
                                built.ready = true;
                            } else {
                                built.ready = false;
                                Log.w(TAG, "failed to initialize text to speech");
                            }
                        }
                    });

            return built;
        }

    }//end Builder

    public static SpeechCommunicatorBuilder builder() {
        return new SpeechCommunicatorBuilder();
    }

    /**
     * Use {@link SpeechCommunicatorBuilder} to construct SpeechCommunicator
     */
    private SpeechCommunicator() {
    }


    private TextToSpeech speech;
    private boolean ready = false;
    private double volume = VOLUME_RANGE.upperEndpoint();

    /**
     * HA-17 indicates if we are in a speaking session
     */
    private boolean speaking = false;

    @Override
    public void write(@Nonnull Class<?> aClass, Type type, @Nullable String message, @Nullable Throwable throwable) {
        if (ready && speaking) {
            if (type.ordinal() >= Type.INFO.ordinal()) {
                speak(message);
            }
        }
    }

    private void speak(@Nullable String message) {
        //http://stackoverflow.com/questions/20296792/tts-utteranceprogresslistener-not-being-called
        HashMap<String, String> map = new HashMap<>();

        final String utteranceId = String.valueOf(System.currentTimeMillis());
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,
                utteranceId);
        map.put(TextToSpeech.Engine.KEY_PARAM_VOLUME, String.valueOf(volume));
        //deprecated method required until sdk 21
        speech.speak(message, TextToSpeech.QUEUE_ADD, map);

    }

    /**
     * Assigns the given choice to the conversation mode to enabled or disable speaking.
     *
     * @param speak true enables conversation mode.
     * @return the existing value before this assignment
     */
    public boolean speakEverything(boolean speak) {
        boolean existing = this.speaking;
        this.speaking = speak;
        return existing;
    }


    /**
     * changes the volume from the additive staying within the range. positive increases volume,
     * negative decreases
     *
     * @param additive some fraction of VOLUME_RANGE.  negite reduces volume, positive increases
     */
    private void volumeAdjusted(double additive) {
        volume += additive;
        volume = RangeExtended.apply(VOLUME_RANGE, volume);
    }

    /**
     * Provides the knowledge of what speaking can do.
     *
     * @return
     */
    public Knowledgeable knowledgeable() {
        KnownAnswers.KnownAnswersBuilder answers = KnownAnswers.builder().subject("speak");
        //speaking
        {
            SimpleQuestion question = new SimpleQuestion("speak out loud");
            ResultsAnswer answer = ResultsAnswer.builder("I'm speaking now").question(
                    question).supplier(
                    new Supplier<Object[]>() {
                        @Override
                        public Object[] get() {
                            speaking = true;
                            return new Object[0];
                        }
                    }).build();
            answers.answer(question, answer);
        }
        //silence
        {
            SimpleQuestion question = new SimpleQuestion("stop speaking");
            ResultsAnswer answer = ResultsAnswer.builder("I will stop speaking").question(
                    question).supplier(
                    new Supplier<Object[]>() {
                        @Override
                        public Object[] get() {
                            speaking = false;
                            return new Object[0];
                        }
                    }).build();
            answers.answer(question, answer);
        }
        //say
        {
            final TrailingSubjectQuestion question = new TrailingSubjectQuestion("say ");
            ResultsAnswer answer = ResultsAnswer.builder().question(
                    question).function(new Function<Question, Object[]>() {
                @Nullable
                @Override
                public Object[] apply(@Nullable Question input) {
                    final String subject = question.subject(input);
                    if (StringUtils.isEmpty(subject)) {
                        return ResultsAnswer.alternateAnswer("say what?");
                    }
                    speak(subject);
                    return new Object[0];
                }
            }).build();
            answers.answer(question, answer);
        }
        //increase volume
        {
            SimpleQuestion question = new SimpleQuestion("speak louder");
            ResultsAnswer answer = ResultsAnswer.builder("speaking louder").question(
                    question).supplier(
                    new Supplier<Object[]>() {
                        @Override
                        public Object[] get() {
                            Object[] result;
                            if (volume >= VOLUME_RANGE.upperEndpoint()) {
                                result = ResultsAnswer.alternateAnswer("I can't speak any louder");
                            } else {
                                volumeAdjusted(0.2);
                                result = new Object[0];
                            }
                            return result;
                        }
                    }).build();
            answers.answer(question, answer);
        }
        //decrease volume
        {
            SimpleQuestion question = new SimpleQuestion("speak softer");
            ResultsAnswer answer = ResultsAnswer.builder("speaking softer").question(
                    question).supplier(
                    new Supplier<Object[]>() {
                        @Override
                        public Object[] get() {
                            Object[] result;
                            if (volume <= VOLUME_RANGE.lowerEndpoint()) {
                                result = ResultsAnswer.alternateAnswer("I can't speak any softer");
                            } else {
                                volumeAdjusted(-0.2);
                                result = new Object[0];
                            }
                            return result;
                        }
                    }).build();
            answers.answer(question, answer);
        }
        //decrease volume
        return answers.build();
    }

    public void shutDown() {
        try {
            this.speech.shutdown();
        } catch (Exception e) {
            Log.e(TAG, "unable to cleanup speech", e);
        }
    }
}
