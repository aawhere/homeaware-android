package com.aawhere.homeaware;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.aawhere.jts.JtsFormat;
import com.aawhere.jts.JtsGeometryUtil;
import com.aawhere.know.KnownAnswers;
import com.aawhere.know.Question;
import com.aawhere.know.ResultsAnswer;
import com.aawhere.know.SimpleQuestion;
import com.aawhere.know.TrailingSubjectChoiceQuestion;
import com.aawhere.know.TrailingSubjectQuestion;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.StringUtilsExtended;
import com.aawhere.measure.QuaternionEulerAngles;
import com.aawhere.robot.comm.Communicator;
import com.aawhere.know.Knowledgeable;
import com.aawhere.know.SimpleAnswer;
import com.aawhere.robot.map.ManualNavigator;
import com.aawhere.robot.map.Navigator;
import com.aawhere.robot.map.nav.HeadingNavigator;
import com.aawhere.robot.map.place.NavigationInstructions;
import com.aawhere.robot.map.place.NavigationObservationTypes;
import com.aawhere.robot.map.place.PlaceNavigator;
import com.aawhere.robot.motion.MotionService;
import com.aawhere.tango.TangoDepthCalculator;
import com.aawhere.tango.TangoDepthDataCsv;
import com.aawhere.tango.TangoUtil;
import com.aawhere.tango.jts.TangoJtsUtil;
import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.TangoAreaDescriptionMetaData;
import com.google.atap.tangoservice.TangoConfig;
import com.google.atap.tangoservice.TangoCoordinateFramePair;
import com.google.atap.tangoservice.TangoEvent;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.vividsolutions.jts.algorithm.Angle;
import com.vividsolutions.jts.geom.Coordinate;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.transform.Result;

/**
 * Using Project Tango, this investigates an area looking to for recognizing features.  Only a
 * single ADF should be available so this will make every attempt to match against that file.
 * <p/>
 * If no ADF is available one is recorded, saved and then the area is matched against that ADF so a
 * new coordinate system is defined.  During such a time
 * <p/>
 * <p/>
 * TODO:Request ADF files from the server based on wifi ssids observed
 * <p/>
 * Created by aroller on 7/23/15.
 */
public class AreaLocalizer {

    private static final Class TAG = AreaLocalizer.class;
    public static final TangoCoordinateFramePair ADF_DEVICE_FRAME_PAIR = new TangoCoordinateFramePair(
            TangoPoseData.COORDINATE_FRAME_AREA_DESCRIPTION,
            TangoPoseData.COORDINATE_FRAME_DEVICE);
    /**
     * used when no adf can be matched, this will create the frame of reference from the start of
     * the service which will be the new origin of the coordinate system.
     */
    public static final TangoCoordinateFramePair ADF_RECORDING_FRAME_PAIR = new TangoCoordinateFramePair(
            TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
            TangoPoseData.COORDINATE_FRAME_DEVICE);
    public static final int MILLIS_FOR_STALE = 1000;
    /**
     * Used for status updates
     */
    private Communicator communicator;
    /**
     * Used to resume and existing area or record a new one.
     */
    private Tango tango;


    /**
     * The ADF file available from the Tango service.
     */
    private String adfId;

    /**
     * The location name is used to identify an adf.
     */
    private String locationName;

    /**
     * Indicates if we are initially creating an area.  True means yes, null means not yet
     * determined.
     */
    private Boolean creating;
    /**
     * The navigator providing instructions about where to command.
     */
    private MotionService navigator;
    private TangoConfig config;


    private LocalizationListener localizationListener;


    /**
     * The latest received from an update.  This is processed during polling thread.
     */
    private Pair<DateTime, TangoXyzIjData> xyzIjData;

    /**
     * The latests received from an update. This is process during polling thread and placed in
     * #pose.
     */
    private Pair<DateTime, TangoPoseData> poseData;
    /**
     * the last observation of tango depth data.
     */
    private Pair<DateTime, Pair<TangoXyzIjData, TangoDepthCalculator>> depth;
    /**
     * The most recent valid observation of pose data.
     */
    private Pair<DateTime, Pair<TangoPoseData, Pair<Coordinate, QuaternionEulerAngles>>> pose;


    /**
     * Used to construct all instances of AreaLocalizer.
     */
    public static class AreaLocalizerBuilder extends ObjectBuilder<AreaLocalizer> {


        private AreaLocalizerBuilder() {
            super(new AreaLocalizer());
        }


        public AreaLocalizerBuilder communicator(Communicator communicator) {
            building.communicator = communicator;
            return this;
        }

        public AreaLocalizerBuilder navigator(MotionService navigator) {
            building.navigator = navigator;
            return this;
        }

        public AreaLocalizerBuilder tango(Tango tango) {
            building.tango = tango;
            return this;
        }

        public AreaLocalizerBuilder listener(LocalizationListener listener) {
            building.localizationListener = listener;
            return this;
        }

        public AreaLocalizerBuilder locationName(String locationName) {
            building.locationName = locationName;
            return this;
        }

        @Override
        protected void validate() {
            super.validate();
            Assertion.assertNotNull("communicator", building.communicator);
            Assertion.assertNotNull("tango", building.tango);
            Assertion.assertNotNull("locationName", building.locationName);

        }

        public AreaLocalizer build() {
            AreaLocalizer built = super.build();
            built.connect();

            return built;
        }

    }//end Builder


    public static AreaLocalizerBuilder builder() {
        return new AreaLocalizerBuilder();
    }

    /**
     * Use {@link AreaLocalizerBuilder} to construct AreaLocalizer
     */
    private AreaLocalizer() {
    }


    public void disconnect() {
        //disconnect has been observed to hang the system
        tango.disconnect();
    }

    /**
     * prepares for new discovery.
     */
    private void discover() {
        resume();
    }

    public void resume() {
        connect();
    }

    private void connect() {

        //keep the old config if it exists...just resume it
        if (this.config == null) {

            //otherwise create a new one.
            this.config = tango.getConfig(TangoConfig.CONFIG_TYPE_CURRENT);

            config.putBoolean(TangoConfig.KEY_BOOLEAN_MOTIONTRACKING, true);
            config.putBoolean(TangoConfig.KEY_BOOLEAN_LEARNINGMODE, true);
            config.putBoolean(TangoConfig.KEY_BOOLEAN_DEPTH, true);
            //ask tango for the latest availability of an adf if not yet assigned
            if (adfId == null) {
                final List<TangoAreaDescriptionMetaData> areaDescriptionMetaDatas = TangoUtil.areaDescriptionMetaDatas(
                        tango);
                ImmutableList<TangoAreaDescriptionMetaData> adfs = ImmutableList.copyOf(
                        Iterables.filter(areaDescriptionMetaDatas,
                                TangoUtil.namePredicate(this.locationName,
                                        TangoUtil.MATCH_WITH_EQUALS)));

                if (!adfs.isEmpty()) {
                    adfId = TangoUtil.uuid(adfs.get(0));
                    if (adfs.size() > 1) {
                        communicator.warn(TAG,
                                "I found multiple area maps for " + this.locationName + " which can cause problems.");
                    }
                }
            }
            if (adfId == null) {
                communicator.info(TAG, "Recording a map for " + this.locationName);
                this.creating = true;
            } else {
                config.putString(TangoConfig.KEY_STRING_AREADESCRIPTION,
                        adfId);
                communicator.info(TAG, "Finding my place in the map for " + this.locationName);
                this.creating = false;
            }
        }

        //listeners must be setup for each configuration session.
        try {
            setUpTangoListeners();
        } catch (Exception e) {
            communicator.error(AreaLocalizer.class, "Failure to initialize tango listeners.",
                    e);
        }
        tango.connect(config);
        if (this.creating) {
            navigator.navigator(localizationNavigator());
        } else {
            //brut force discovery since updates won't happen on their own
            //Does tango need some downtime to stablize before moving?
            navigator.navigator(ManualNavigator.builder().instructions(
                    NavigationInstructions.builder().instruction(
                            PlaceNavigator.Instruction.RIGHT).build()).build());
        }
    }

    private void setUpTangoListeners() {

        // Set Tango Listeners for Poses Device wrt Start of Service, Device wrt
        // ADF and Start of Service wrt ADF
        ArrayList<TangoCoordinateFramePair> framePairs = new ArrayList<TangoCoordinateFramePair>();

        if (isCreating()) {
            framePairs.add(ADF_RECORDING_FRAME_PAIR);
        } else {
            //this only listens for events against the known adf
            framePairs.add(ADF_DEVICE_FRAME_PAIR);
        }

        pollAndProcess();


        tango.connectListener(framePairs, new Tango.OnTangoUpdateListener() {


            /**
             * Only called during ADF discovery.  Recording does not
             * register depth.
             *
             * @param xyzij
             */
            @Override
            public void onXyzIjAvailable(TangoXyzIjData xyzij) {
                //processed later during polling
                AreaLocalizer.this.xyzIjData = Pair.of(new DateTime(), xyzij);
            }

            @Override
            public void onFrameAvailable(int i) {
                //not used
            }

            // Listen to Tango Events
            @Override
            public void onTangoEvent(final TangoEvent event) {
                if (event != null) {
                    switch (event.eventType) {
                        case TangoEvent.EVENT_UNKNOWN:
                        case TangoEvent.EVENT_GENERAL:
                        case TangoEvent.EVENT_FEATURE_TRACKING:
                        case TangoEvent.EVENT_IMU:
                            if (event.eventKey != null) {
                                communicator.verbose(TAG, "TangoEvent: " +
                                        event.eventKey + " " + event.eventType + " " + event.eventValue);
                            }
                            break;
                        default:
                            //do nothing
                    }

                }
            }

            @Override
            public void onPoseAvailable(TangoPoseData pose) {

                if (pose.statusCode == TangoPoseData.POSE_VALID) {
                    //assign it to the pending for processing
                    poseData = Pair.of(DateTime.now(), pose);
                }
            }
        });

    }

    /**
     * Executes a background thread to repeat looking for new data and processing it on a thread
     * differen than tango.
     */
    private void pollAndProcess() {
        new Thread(new Runnable() {
            Boolean adfMatchNotified = false;

            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        Log.e(TAG.getName(), "error pausing polling thread", e);
                        Thread.currentThread().interrupt();
                        throw new RuntimeException(e);
                    }

                    Coordinate coordinate = null;
                    Double heading = null;
                    Double metersClear = null;

                    //pose
                    {
                        Pair<DateTime, TangoPoseData> poseData = AreaLocalizer.this.poseData;
                        if (poseData != null) {
                            //intentially blocking by name and using locally to avoid clobbering
                            Pair<DateTime, Pair<TangoPoseData, Pair<Coordinate, QuaternionEulerAngles>>> pose = AreaLocalizer.this.pose;
                            if (pose == null || poseData.getRight() != pose.getRight().getLeft()) {
                                TangoPoseData pendingPose = poseData.getRight();
                                final QuaternionEulerAngles quaternionEulerAngles = TangoUtil.headingAngles(
                                        pendingPose);

                                heading = quaternionEulerAngles.heading();
                                coordinate = TangoJtsUtil.coordinate(pendingPose);

                                Log.v(TAG.getName(),
                                        "fresh pose " + NavigationObservationTypes.headingFormatted(
                                                heading) + " from " + JtsFormat.brief(
                                                coordinate));

                                //FIXME:there must be somewhere we can do this once
                                if (pose == null && adfId != null) {
                                    synchronized (adfMatchNotified) {
                                        if (!adfMatchNotified) {
                                            final TangoAreaDescriptionMetaData areaDescriptionMetaData = tango.loadAreaDescriptionMetaData(
                                                    AreaLocalizer.this.adfId);
                                            localizationListener.adfMatched(areaDescriptionMetaData,
                                                    coordinate);
                                            adfMatchNotified = true;
                                            //we found a match, but we need to improve our exact location by looking around
                                            //the turning navigator will work now that heading is valid
                                            navigator.navigator(localizationNavigator());
                                        }
                                    }
                                }
                                //save this for on-demand use
                                AreaLocalizer.this.pose = Pair.of(poseData.getLeft(),
                                        Pair.of(pendingPose,
                                                Pair.of(coordinate, quaternionEulerAngles)));
                            } else {
                                //use the existing if still fresh
                                if (poseIsFresh(pose)) {
                                    coordinate = pose.getRight().getRight().getLeft();
                                    heading = pose.getRight().getRight().getRight().heading();
                                }
                            }
                        }

                    }

                    //xyzij
                    {
                        Pair<DateTime, TangoXyzIjData> xyzIjData = AreaLocalizer.this.xyzIjData;
                        if (xyzIjData != null) {
                            Pair<DateTime, Pair<TangoXyzIjData, TangoDepthCalculator>> depth = AreaLocalizer.this.depth;
                            //process if not yet processed by comparing for same
                            if (depth == null || depth.getRight().getLeft() != xyzIjData.getRight()) {
                                final TangoDepthCalculator calculator = TangoDepthCalculator.builder().data(
                                        xyzIjData.getRight()).build();
                                if (calculator.isConfident()) {
                                    metersClear = calculator.minDistanceInMeters();
                                    Log.v(TAG.getName(),
                                            "fresh meters clear " + NavigationObservationTypes.distanceFormatted(
                                                    metersClear));
                                }
                                AreaLocalizer.this.depth = Pair.of(xyzIjData.getLeft(), Pair.of(
                                        xyzIjData.getRight(), calculator));

                            } else {
                                if (depthIsFresh(depth)) {
                                    metersClear = depth.getRight().getRight().minDistanceInMeters();

                                }
                            }
                        }
                    }


                    //all may be null and may be mixed times, but guaranteed fresh
                    //it will poll consistently so navigators can rely on timing
                    navigator.instruction(coordinate, heading, metersClear);

                }
            }
        }).start();
    }


    /**
     * Returns the heading, clockwise positive from Y axis, or null if not fresh.
     *
     * @return
     */
    public Double headingInRadians() {
        if (poseIsFresh()) {
            return pose.getRight().getRight().getRight().heading();
        } else {
            return null;
        }
    }

    public void save() {

        final String adfIdSaved = this.tango.saveAreaDescription();
        //warn if the ids don't match indicating multiple saves have been made during a learning session
        if (adfId != null && !adfId.equals(adfIdSaved)) {
            communicator.warn(TAG, "Oops.  I'm saving the map to the wrong destination.");
            communicator.verbose(TAG, adfIdSaved + " saved, but expected " + adfId);
        }

        if (isCreating()) {
            //assign a name so we can find this later.
            final TangoAreaDescriptionMetaData areaDescriptionMetaData = tango.loadAreaDescriptionMetaData(
                    adfIdSaved);
            areaDescriptionMetaData.set(TangoAreaDescriptionMetaData.KEY_NAME,
                    this.locationName.getBytes());
            tango.saveAreaDescriptionMetadata(adfIdSaved, areaDescriptionMetaData);
            //tango must receive a new config with the newly recorded file
            this.config = null;
            disconnect();
            connect();
        }
        this.adfId = adfIdSaved;
    }

    /**
     * @return true if this is the initial creating, false if attempting to locate against a map,
     * null if not yet determined.
     */
    public Boolean isCreating() {
        return this.creating;
    }

    /**
     * Provides the most recent known coordinate if it is not stale.
     */
    @Nullable
    public Coordinate coordinate() {
        if (poseIsFresh()) {
            return pose.getRight().getRight().getLeft();
        }
        return null;
    }

    public boolean poseIsFresh() {
        final Pair<DateTime, Pair<TangoPoseData, Pair<Coordinate, QuaternionEulerAngles>>> pose = this.pose;
        return poseIsFresh(pose);
    }

    private boolean poseIsFresh(Pair<DateTime, Pair<TangoPoseData, Pair<Coordinate, QuaternionEulerAngles>>> pose) {
        return pose != null && pose.getLeft().isAfter(DateTime.now().minusMillis(MILLIS_FOR_STALE));
    }

    /**
     * Provides the coordinate observed at the given timestamp.  This is only valid for coordinates
     * of the localized ADF.
     *
     * @return the coordinate of the location closest to the known time or null if not found.
     */
    @Nullable
    public Coordinate coordinate(DateTime timestamp) {
        return coordinate();
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        disconnect();
    }

    public static interface LocalizationListener {

        /**
         * Called once and only once for the listener registered to be notified when an ADF file has
         * been matched.
         *
         * @param areaDescriptionMetaData
         */
        public void adfMatched(final TangoAreaDescriptionMetaData areaDescriptionMetaData, Coordinate currentLocation);
    }

    /**
     * @return the depth only if it is fresh, otherwise null
     */
    private Double depth() {
        if (depthIsFresh()) {
            final TangoDepthCalculator depthCalculator = depthCalculator();
            if (depthCalculator.isConfident()) {
                return depthCalculator.minDistanceInMeters();
            }
        }
        return null;
    }

    private TangoDepthCalculator depthCalculator() {
        return this.depth.getRight().getRight();
    }

    private boolean depthIsFresh() {
        final Pair<DateTime, Pair<TangoXyzIjData, TangoDepthCalculator>> depth = this.depth;
        return depthIsFresh(depth);
    }

    private boolean depthIsFresh(Pair<DateTime, Pair<TangoXyzIjData, TangoDepthCalculator>> depth) {
        return depth != null && depth.getRight().getRight().isConfident() && depth.getLeft().isAfter(
                DateTime.now().minusSeconds(1));
    }

    public Knowledgeable knowledgeable() {
        final KnownAnswers.KnownAnswersBuilder knownAnswersBuilder = KnownAnswers.builder();

        {
            final SimpleQuestion coordinatesQuestion = new SimpleQuestion(
                    "what are your coordinates");
            Supplier<Object[]> coordinateSupplier = new Supplier<Object[]>() {

                @Override
                public Object[] get() {
                    final Coordinate coordinate = coordinate(DateTime.now());
                    String coordinateValue;
                    if (coordinate != null) {
                        coordinateValue = JtsFormat.brief(coordinate);
                    } else {
                        coordinateValue = "unknown coordinates";
                    }
                    return new Object[]{coordinateValue};
                }
            };
            final ResultsAnswer coordinatesAnswer = ResultsAnswer.builder("I'm at %s").supplier(
                    coordinateSupplier).question(
                    coordinatesQuestion).build();
            knownAnswersBuilder.answer(
                    coordinatesQuestion, coordinatesAnswer);
        }

        //save the area description
        {
            SimpleQuestion question = new SimpleQuestion("save your area memory");
            final ResultsAnswer answer = ResultsAnswer.builder(
                    "I updated the map with all I've seen lately.").question(question).supplier(
                    new Supplier<Object[]>() {
                        @Override
                        public Object[] get() {
                            save();
                            return new Object[0];
                        }
                    }).build();
            knownAnswersBuilder.answer(question, answer);
        }


        //HEADING
        {
            final SimpleQuestion headingQuestion = new SimpleQuestion("what is your heading");
            Supplier<Object[]> headingSupplier = new Supplier<Object[]>() {
                @Override
                public Object[] get() {

                    final Double headingInRadians = headingInRadians();
                    if (headingInRadians != null) {
                        return ResultsAnswer.results(
                                NavigationObservationTypes.headingFormatted(headingInRadians));
                    } else {
                        return ResultsAnswer.alternateAnswer("I'm a little lost right now");
                    }
                }
            };

            final ResultsAnswer headingAnswer = ResultsAnswer.builder(
                    "I'm heading %s degrees from y").question(headingQuestion).supplier(
                    headingSupplier).build();
            knownAnswersBuilder.answer(headingQuestion, headingAnswer);
        }

        final KnownAnswers knownAnswers = knownAnswersBuilder.subject("locate myself").build();
        return knownAnswers;
    }

    @NonNull
    private Object[] depthNotSeenAnswer() {
        return new Object[]{new SimpleAnswer(
                "I haven't seen depth this session")};
    }

    private Object[] depthLastSeenAnswer() {
        if (depth != null) {
            final DateTime time = depth.getLeft();
            return ResultsAnswer.alternateAnswer(
                    "I haven't seen depth for " + NavigationObservationTypes.millisFormatted(
                            new Duration(time, DateTime.now()).getMillis()));
        } else {
            return depthNotSeenAnswer();
        }

    }

    public Knowledgeable lookKnowledge(final File directory) {
        final KnownAnswers.KnownAnswersBuilder knownAnswersBuilder = KnownAnswers.builder().subject(
                "see");
        //look in a relative direction
        {
            final ImmutableMap<String, HeadingNavigator.Relative> byName = Maps.uniqueIndex(
                    Lists.newArrayList(HeadingNavigator.Relative.values()),
                    Functions.toStringFunction());
            final TrailingSubjectChoiceQuestion relativeQuestion = TrailingSubjectChoiceQuestion.builderWithChoices(
                    "look ").subject("relative direction").choices(byName.keySet()).build();
            Function<Question, Object[]> relativeFunction = new Function<Question, Object[]>() {

                @Override
                public Object[] apply(Question input) {
                    //direction is available with valid coordinates...during recording or after localization
                    if (isCreating() || (!isCreating() && adfId != null)) {

                        String where = relativeQuestion.subject(input);
                        if (StringUtils.isNoneEmpty(where)) {
                            String bestMatch = StringUtilsExtended.bestMatch(where,
                                    byName.keySet());
                            if (bestMatch != null) {
                                HeadingNavigator.Relative relative = byName.get(bestMatch);
                                navigator.navigator(relative.navigator());
                                return ResultsAnswer.results(bestMatch);
                            } else {
                                return ResultsAnswer.alternateAnswer(
                                        "I don't know how to look " + where);
                            }
                        } else {
                            return ResultsAnswer.alternateAnswer("look where?");
                        }
                    } else {
                        return ResultsAnswer.alternateAnswer(
                                "I am still trying to figure out where I'm located");
                    }
                }
            };
            ResultsAnswer relativeAnswer = ResultsAnswer.builder("looking %s").question(
                    relativeQuestion)
                    .function(relativeFunction).build();
            knownAnswersBuilder.answer(relativeQuestion,
                    relativeAnswer);
        }

        //look at a heading
        {
            final TrailingSubjectQuestion question = TrailingSubjectQuestion.builder(
                    "look at heading ").subject("degrees").build();
            final Function<Question, Object[]> function = new Function<Question, Object[]>() {
                @Nullable
                @Override
                public Object[] apply(@Nullable Question input) {
                    String subject = question.subject(input);
                    try {
                        Double degrees = Double.parseDouble(subject);
                        HeadingNavigator headingNavigator = HeadingNavigator.builder().targetHeading(
                                JtsGeometryUtil.headingInRadians(degrees)).build();
                        AreaLocalizer.this.navigator.navigator(headingNavigator);
                        return ResultsAnswer.results(subject);
                    } catch (NumberFormatException e) {
                        return ResultsAnswer.alternateAnswer(
                                "provide the heading in integer degrees");
                    }
                }
            };
            final ResultsAnswer answer = ResultsAnswer.builder("looking at heading %s°").question(
                    question).function(function).build();
            knownAnswersBuilder.answer(question, answer);

        }

        //look at coordinates
        {
            final TrailingSubjectQuestion question = TrailingSubjectQuestion.builder(
                    "look at coordinates ").subject("x,y").build();
            Function<Question, Object[]> function = new Function<Question, Object[]>() {
                @Nullable
                @Override
                public Object[] apply(@Nullable Question input) {
                    final String subject = question.subject(input);
                    final Coordinate coordinate = JtsFormat.parseCoordinate(subject);
                    if (coordinate != null) {
                        navigator.navigator(HeadingNavigator.turnTo(coordinate));
                        return ResultsAnswer.results(subject);
                    } else {
                        return ResultsAnswer.alternateAnswer(
                                subject + " must be a coordinate x,y");
                    }
                }
            };
            ResultsAnswer answer = ResultsAnswer.builder("looking at coordinates %s").question(
                    question).function(function).build();
            knownAnswersBuilder.answer(question, answer);
        }


        //clear distance in front
        {
            final SimpleQuestion distanceQuestion = new SimpleQuestion(
                    "what is your clear distance");
            Supplier<Object[]> distanceSupplier = new Supplier<Object[]>() {
                @Override
                public Object[] get() {
                    Object[] results;
                    if (AreaLocalizer.this.depth != null) {
                        //ensure the data is not stale
                        if (depthIsFresh()) {
                            final TangoDepthCalculator calculator = depthCalculator();
                            if (calculator.isClear()) {
                                results = new Object[]{new SimpleAnswer(
                                        "The path in front of me is clear")};

                            } else {
                                if (calculator.isConfident()) {
                                    double meters = calculator.minDistanceInMeters();
                                    results = new Object[]{TangoDepthCalculator.METER_FORMAT.format(
                                            meters)};
                                } else {
                                    results = ResultsAnswer.alternateAnswer(
                                            "I'm having trouble seeing right now");
                                }

                            }
                        } else {
                            results = depthLastSeenAnswer();
                        }
                    } else {
                        results = depthNotSeenAnswer();
                    }
                    return results;
                }
            };
            ResultsAnswer distanceAnswer = ResultsAnswer.builder(
                    "I can see something %s meters straight ahead").supplier(
                    distanceSupplier).question(distanceQuestion).build();
            knownAnswersBuilder.answer(distanceQuestion, distanceAnswer);
        }

        //when did you see depth last
        {
            SimpleQuestion question = new SimpleQuestion("when did you last see depth");
            Supplier<Object[]> supplier = new Supplier<Object[]>() {
                @Override
                public Object[] get() {
                    return depthLastSeenAnswer();
                }
            };
            knownAnswersBuilder.answer(question,
                    ResultsAnswer.builder().question(question).supplier(supplier).build());
        }

        //point cloud point count
        {
            final SimpleQuestion question = new SimpleQuestion("how many points can you see");
            Supplier<Object[]> supplier = new Supplier<Object[]>() {
                @Override
                public Object[] get() {
                    if (xyzIjData != null) {
                        final String millisFormatted = NavigationObservationTypes.millisFormatted(
                                new Duration(xyzIjData.getKey(), DateTime.now()).getMillis());
                        return ResultsAnswer.results(xyzIjData.getRight().xyzCount,
                                millisFormatted);
                    } else {
                        return ResultsAnswer.alternateAnswer("I don't have depth data");
                    }
                }
            };
            ResultsAnswer answer = ResultsAnswer.builder("I saw %s points %s ago").question(
                    question).supplier(supplier).build();
            knownAnswersBuilder.answer(question, answer);
        }

        //save a point cloud
        {
            final TrailingSubjectQuestion question = TrailingSubjectQuestion.builder(
                    "save the point cloud to ").subject("file name").build();
            Function<Question, Object[]> function = new Function<Question, Object[]>() {
                @Nullable
                @Override
                public Object[] apply(@Nullable Question input) {
                    final String subject = question.subject(input);
                    if (StringUtils.isNotEmpty(subject)) {
                        final TangoDepthDataCsv tangoDepthDataCsv = TangoDepthDataCsv.builder().name(
                                subject).data(depth.getRight().getLeft()).directory(
                                directory).build();
                        if (tangoDepthDataCsv.error() == null) {
                            return ResultsAnswer.results(tangoDepthDataCsv.file());
                        } else {
                            return ResultsAnswer.alternateAnswer(
                                    "failed to save point cloud because " + tangoDepthDataCsv.error().getMessage());
                        }
                    } else {
                        return ResultsAnswer.alternateAnswer(
                                "provide a descriptive name of what is being seen");
                    }
                }
            };
            knownAnswersBuilder.answer(question,
                    ResultsAnswer.builder("saved point cloud to %s").question(question).function(
                            function).build());
        }
        return knownAnswersBuilder.build();
    }

    /**
     * Navigator specializing in learning or recognizing an area being friendly to Tango.
     *
     * @return
     */
    private Navigator localizationNavigator() {
        return new Navigator() {

            private long timestampLastMoved = Long.MAX_VALUE;
            private int millisAllowedForViewing = 5000;
            LinkedList<Navigator> navigatorQueue = Lists.newLinkedList();
            private Navigator delegate;

            {
                //should make a full circle left
                navigatorQueue.add(HeadingNavigator.Relative.SLIGHT_LEFT.navigator());
                navigatorQueue.add(HeadingNavigator.Relative.SLIGHT_RIGHT.navigator());
                navigatorQueue.add(HeadingNavigator.Relative.SLIGHT_RIGHT.navigator());
                navigatorQueue.add(HeadingNavigator.Relative.LEFT.navigator());
                navigatorQueue.add(HeadingNavigator.Relative.SLIGHT_LEFT.navigator());
                navigatorQueue.add(HeadingNavigator.Relative.RIGHT.navigator());
                navigatorQueue.add(HeadingNavigator.Relative.SLIGHT_RIGHT.navigator());
                navigatorQueue.add(HeadingNavigator.Relative.SLIGHT_LEFT.navigator());
                navigatorQueue.add(HeadingNavigator.Relative.SHARP_LEFT.navigator());
                navigatorQueue.add(HeadingNavigator.Relative.SLIGHT_RIGHT.navigator());
                navigatorQueue.add(HeadingNavigator.Relative.LEFT.navigator());
                navigatorQueue.add(HeadingNavigator.Relative.SLIGHT_RIGHT.navigator());
                navigatorQueue.add(HeadingNavigator.Relative.SHARP_LEFT.navigator());

            }

            @Nonnull
            @Override
            public NavigationInstructions instruction(Coordinate coordinate, Double headingInRadians, Double clearDistanceMeters) {


                if (coordinate != null) {
                    //initialize the time only when we get the first valid signal
                    if (timestampLastMoved == Long.MAX_VALUE) {
                        timestampLastMoved = System.currentTimeMillis();
                        Log.i(TAG.getSimpleName(), "initializing timestamp");
                    }
                }
                long millisSinceMoved = System.currentTimeMillis() - timestampLastMoved;
                if (millisSinceMoved > millisAllowedForViewing) {
                    timestampLastMoved = System.currentTimeMillis();
                    if (navigatorQueue.isEmpty()) {
                        delegate = null;
                        if (isCreating()) {
                            if (coordinate != null) {
                                //we should be at the origin.  if too far then it's a bad session
                                final double distanceToOrigin = coordinate.distance(
                                        new Coordinate(0, 0));
                                double maxDistanceFromOriginToSave = 0.3;

                                if (distanceToOrigin < maxDistanceFromOriginToSave) {
                                    save();
                                } else {
                                    communicator.warn(TAG,
                                            "the map recording is not accurate enough to save since I am " + NavigationObservationTypes.distanceFormatted(
                                                    distanceToOrigin) + " away from the origin");
                                }
                            } else {
                                communicator.warn(TAG,
                                        "the map recording is not accurate enough to save since the current location is not known");
                            }
                        } else {
                            if (adfId != null) {
                                save();
                            }
                        }
                    } else {
                        delegate = navigatorQueue.poll();
                    }
                }

                if (delegate != null) {
                    return delegate.instruction(coordinate, headingInRadians, clearDistanceMeters);
                } else {
                    return null;
                }
                //react on events

            }
        };
    }
}
