package com.aawhere.homeaware;

import android.util.Log;

import com.aawhere.robot.comm.Communicator;

import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Implements the Communicator writing to the standard android Log useful for debugging by
 * developers.
 */
public class LogCommunicator extends Communicator {

    @Override
    public void write(@Nonnull Class tag, @Nonnull Type type, @Nullable String message, @Nullable Throwable error) {
        message = (message == null) ? StringUtils.EMPTY : message;
        int severity;
        switch (type) {
            case ERROR:
                severity = Log.ERROR;
                break;
            case VERBOSE:
                severity = Log.VERBOSE;
                break;
            case INFO:
                severity = Log.INFO;
                break;
            case WARN:
                severity = Log.WARN;
                break;
            default:
                throw new RuntimeException(type + " not handled");

        }

        if (type == Type.ERROR && error != null) {
            Log.e(tag(tag), message, error);
        } else {
            Log.println(severity, tag(tag), message);
        }
    }
}
