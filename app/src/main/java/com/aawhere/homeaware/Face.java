package com.aawhere.homeaware;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.UtteranceProgressListener;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.aawhere.homeaware.util.SystemUiHider;
import com.aawhere.jts.JtsFormat;
import com.aawhere.know.Answer;
import com.aawhere.know.Answers;
import com.aawhere.know.KnowledgeBase;
import com.aawhere.know.Knowledgeable;
import com.aawhere.know.KnownAnswers;
import com.aawhere.know.Question;
import com.aawhere.know.ResultsAnswer;
import com.aawhere.know.SimpleAnswer;
import com.aawhere.know.SimpleQuestion;
import com.aawhere.know.TrailingSubjectQuestion;
import com.aawhere.robot.camera.CameraService;
import com.aawhere.robot.comm.Communicator;
import com.aawhere.robot.comm.Communicators;
import com.aawhere.robot.comm.SpeechCommunicator;
import com.aawhere.robot.comm.SphinxSpeechInput;
import com.aawhere.robot.irobot.IRobotCreate2Mover;
import com.aawhere.robot.map.ManualNavigator;
import com.aawhere.robot.map.MapDiscoveryService;
import com.aawhere.robot.map.MapService;
import com.aawhere.robot.map.nav.HeadingNavigator;
import com.aawhere.robot.map.place.NavigationInstructions;
import com.aawhere.robot.map.place.NavigationObservationTypes;
import com.aawhere.robot.map.place.Place;
import com.aawhere.robot.map.place.PlaceNavigator;
import com.aawhere.robot.motion.MotionService;
import com.aawhere.robot.motion.Mover;
import com.aawhere.tango.TangoDepthCalculator;
import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.TangoAreaDescriptionMetaData;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.ullink.slack.simpleslackapi.SlackChannel;
import com.ullink.slack.simpleslackapi.SlackMessageHandle;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.events.SlackMessagePosted;
import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;
import com.ullink.slack.simpleslackapi.listeners.SlackMessagePostedListener;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;

import net.danlew.android.joda.JodaTimeAndroid;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;


/**
 * An example full-screen activity that shows and hides the system UI (i.e. status bar and
 * navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class Face extends Activity {
    private static final String TAG = Face.class.getName();

    /**
     * Whether or not the system UI should be auto-hidden after {@link #AUTO_HIDE_DELAY_MILLIS}
     * milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after user interaction
     * before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise, will show the
     * system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;
    public static final String SLACKBOT_API_TOKEN_KEY = "slackbotApiToken";
    public static final String LOCATION_NAME_KEY = "locationName";

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;
    private AreaLocalizer localizer;

    private MotionService motionService;
    private Communicator communicator;
    private TextView screenMessage;
    /**
     * indicates if slack has been succesffully configured and communication confirmed.
     */
    private Boolean slackConnected;
    private Tango tango;

    //location name should match the slack room if configured
    private String locationName;
    private String robotName = "home aware";

    private KnowledgeBase knowledgeBase = KnowledgeBase.builder().build();

    /**
     * Manages the map once localization has happened.
     */
    private MapService mapService;
    /**
     * allows screen input as a form of communication.
     */
    private EditText screenTextInput;

    /**
     * Use this to interact with regular cameras.
     */
    private CameraService cameraService;
    private SphinxSpeechInput speechInput;
    private SpeechCommunicator speechCommunicator;
    //private SpeechInput speechInput;


    private enum Brightness {
        DEFAULT(-1), SHOW(1), DIM(0.1);
        public final float level;

        Brightness(double level) {
            this.level = (float) level;
        }

    }

    @Override
    public void attachBaseContext(Context base) {
        MultiDex.install(base);
        super.attachBaseContext(base);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JodaTimeAndroid.init(this);

        setContentView(R.layout.activity_face);

        final View controlsView = findViewById(R.id.fullscreen_content_controls);

        final View contentView = findViewById(R.id.fullscreen_content);


        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });


        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });


        //================== App Initialization ============================

        locationName = getPreferences(MODE_PRIVATE).getString(LOCATION_NAME_KEY, "Here");

        //setup communicator
        communicatorsInitialized();

        tango = new Tango(this);

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        final View goButton = findViewById(R.id.go_button);
        goButton.setOnTouchListener(mDelayHideTouchListener);

        this.screenMessage = (TextView) findViewById(R.id.fullscreen_content);


        this.screenTextInput = (EditText) findViewById(R.id.text_input);
        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String message = screenTextInput.getText().toString();
                handleMessage(message);
                screenTextInput.clearComposingText();
                screenTextInput.clearFocus();
                screenTextInput.setText(StringUtils.EMPTY);
                hideKeyboard();
            }
        });

        hideKeyboard();
        knowSlackSetup();
        knowTango();
        motionServiceInitialized();
        //photo taking needs work HA-4
        //cameraSetup();

    }


    /**
     * Configures the app with the various modes of communication (screen, slack, speech, etc).
     */
    private void communicatorsInitialized() {
        final Communicators.TranscriptionsBuilder communicatorsBuilder = Communicators.builder();


        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        String slackBotApiToken = preferences.getString(SLACKBOT_API_TOKEN_KEY, null);

        if (slackBotApiToken != null) {
            communicatorsBuilder.transcription(
                    new SlackCommunicator(locationName, slackBotApiToken
                    ));
        }

        {
            UtteranceProgressListener speechProgressListener = new UtteranceProgressListener() {
                @Override
                public void onStart(String utteranceId) {
                    Log.w(TAG, "speaking started " + utteranceId);
                    speechInput.stopListening();
                }

                @Override
                public void onDone(String utteranceId) {
                    Log.w(TAG, "done speaking " + utteranceId);
                    speechInput.listenForKnowledge();
                }

                @Override
                public void onError(String utteranceId) {
                    Log.w(TAG, "failed to speak " + utteranceId);
                    speechInput.listenForKnowledge();
                }
            };
            //causing Activity com.aawhere.homeaware.Face has leaked ServiceConnection android.speech.tts.TextToSpeech$Connection@64929b98 that was originally bound here
            Face.this.speechCommunicator = SpeechCommunicator.builder().listener(
                    speechProgressListener).context(
                    this).build();
            know(speechCommunicator.knowledgeable());
            communicatorsBuilder.transcription(speechCommunicator);
        }
        communicator = communicatorsBuilder.transcription(
                new LogCommunicator()).transcription(
                this.new ScreenCommunicator()).build();

        //Google speech input is on hold in favor of offline
        //this.speechInput = new SpeechInput();

    }

    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * Initializes motion service preparing it for manual navigation.
     */
    public void motionServiceInitialized() {
        if (motionService == null) {
            //setup the motionService to move the robot or the screen if not
            final UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
            IRobotCreate2Mover iRobotCreate2Mover = IRobotCreate2Mover.builder().manager(
                    usbManager).build();
            Mover mover;
            if (iRobotCreate2Mover.isReady()) {
                mover = iRobotCreate2Mover;
                know(iRobotCreate2Mover.knowledgeable());

                communicator.info(getClass(), iRobotCreate2Mover.describe());

                //if using robot and slack then dim the screen to save battery
                if (BooleanUtils.isTrue(slackConnected)) {
                    display(Brightness.DIM);
                }
            } else {
                mover = new ScreenMover();
                communicator.warn(getClass(), iRobotCreate2Mover.describe());
            }

            motionService = MotionService.builder().navigator(
                    ManualNavigator.builder().instructions(
                            NavigationInstructions.builder().instruction(
                                    PlaceNavigator.Instruction.STOP).build()).build()).communicator(
                    communicator).mover(mover).build();
            know(motionService.knowledgeable());
            //now that we have motion service, provide it to irobot for navigation knowledge
            if (mover instanceof IRobotCreate2Mover) {
                know(iRobotCreate2Mover.knowledgeAboutMoving(motionService));
            }

            //localizing here is annoying during development
            //discover();
        }
    }

    private void know(Knowledgeable knowledgeable) {
        if (knowledgeable != null) {
            KnowledgeBase.mutator(knowledgeBase).know(knowledgeable).build();
        }
    }


    /**
     * sets the display level to match the mode.
     */
    private void display(Brightness brightness) {
        WindowManager.LayoutParams layout = getWindow().getAttributes();
        layout.screenBrightness = brightness.level;
        getWindow().setAttributes(layout);
    }

    private void discover() {


        if (Tango.hasPermission(this, Tango.PERMISSIONTYPE_MOTION_TRACKING) && Tango.hasPermission(
                this, Tango.PERMISSIONTYPE_ADF_LOAD_SAVE)) {

            if (this.localizer == null) {
                motionServiceInitialized();


                AreaLocalizer.LocalizationListener listener = new AreaLocalizer.LocalizationListener() {


                    @Override
                    public void adfMatched(TangoAreaDescriptionMetaData areaDescriptionMetaData, Coordinate currentLocation) {

                        Face.this.mapService = new MapService(
                                new String(areaDescriptionMetaData.get(
                                        TangoAreaDescriptionMetaData.KEY_UUID)), getFilesDir());
                        know(Face.this.mapService.knowledgeable(motionService));
                        knowMapNavigation();
                        MapDiscoveryService discoveryService = MapDiscoveryService.builder().mapService(
                                mapService).motionService(motionService).build();
                        know(discoveryService.knowledge());
                        final Place nearestPlace = Face.this.mapService.nearestPlace(
                                currentLocation);

                        motionService.navigator(ManualNavigator.builder().instructions(
                                NavigationInstructions.builder().instruction(
                                        PlaceNavigator.Instruction.STOP).build()).build());

                        if (nearestPlace != null) {
                            //TODO: ask for a name of the current place if nothing nearby
                            distanceFromCommunciated(currentLocation, nearestPlace);
                        } else {
                            mapService.place("Origin", new Coordinate(0, 0, 0));
                            communicator.info(getClass(),
                                    "I found my place in the map.  Mark some interesting places and we'll build a map of " + Face.this.locationName);
                            //TODO:provide the discovery mapper
                            motionService.navigator(ManualNavigator.builder().instructions(
                                    NavigationInstructions.builder().instruction(
                                            PlaceNavigator.Instruction.STOP).build()).build());
                        }
                    }


                };
                this.localizer = AreaLocalizer.builder().tango(tango).communicator(
                        communicator).navigator(
                        motionService).locationName(locationName).listener(listener).build();
                know(this.localizer.knowledgeable());
                know(this.localizer.lookKnowledge(
                        getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)));
                knowNavigation();
            } else {
                localizer.resume();
            }


        } else {
            communicator.warn(getClass(),
                    "I prefer not to be touched, but permission is required to continue...please grant on my screen");
            //prompt now for tango permissions
            startActivityForResult(
                    Tango.getRequestPermissionIntent(Tango.PERMISSIONTYPE_MOTION_TRACKING),
                    Tango.TANGO_INTENT_ACTIVITYCODE);

            startActivityForResult(
                    Tango.getRequestPermissionIntent(Tango.PERMISSIONTYPE_ADF_LOAD_SAVE),
                    Tango.TANGO_INTENT_ACTIVITYCODE);
        }
    }


    private double distanceFromCommunciated(Coordinate currentLocation, Place place) {
        final double distance = place.point().getCoordinate().distance(
                currentLocation);
        communicator.info(getClass(),
                "I am " + TangoDepthCalculator.METER_FORMAT.format(
                        distance) + " meters away from " + place.name());
        return distance;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (localizer != null) {
            localizer.disconnect();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (localizer != null) {
            discover();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        display(Brightness.DEFAULT);
        if (localizer != null) {
            localizer.disconnect();
        }
        if (this.speechInput != null) {
            speechInput.shutDown();
        }
        if (this.speechCommunicator != null) {
            speechCommunicator.shutDown();
        }
    }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the system UI. This is to
     * prevent the jarring behavior of controls going away while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any previously scheduled
     * calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    /**
     * Displays high level messages to the screen.
     */
    public class ScreenCommunicator extends Communicator {

        @Override
        public void write(@Nonnull Class tag, Type type, final @Nullable String message, @Nullable Throwable error) {
            if (screenMessage != null && type.ordinal() > Type.VERBOSE.ordinal()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        screenMessage.setText(message);
                    }
                });
            }
        }
    }

    /**
     * A mover that doesn't actually command the robot, but displays how to command to the screen
     * output.
     */
    public class ScreenMover implements Mover {
        private String description = "Not yet initialized";

        @Override
        public Result move(final NavigationInstructions navigationInstructions) {
            if (screenMessage != null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        screenMessage.setText(navigationInstructions.toString());
                    }
                });
                description = navigationInstructions.toString();
                return Result.SUCCESS;
            } else {
                return Result.FAILURE;
            }
        }

        @Override
        public String describe() {
            return description;
        }
    }

    /**
     * Asynchronously sets up speech listening.  This must be done after the robot's name has been
     * assigned.
     */
    private void setupSpeechInput() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final SphinxSpeechInput.MessageReceivedListener listener = new SphinxSpeechInput.MessageReceivedListener() {
                        @Override
                        public void messageReceived(String message) {
                            //go into conversation mode before the message is sent
                            Face.this.speechCommunicator.speakEverything(true);
                            handleMessage(message);

                        }

                        @Override
                        public void listeningStopped() {
                            //stop speaking until another message has been received.
                            Face.this.speechCommunicator.speakEverything(false);
                        }
                    };

                    Face.this.speechInput = SphinxSpeechInput.builder().context(
                            Face.this).activationPhrase(
                            Face.this.robotName).knowledgeBase(
                            knowledgeBase).communicator(communicator).listener(
                            listener).buildVerbose();
                    know(Face.this.speechInput.knowledgeable());
                } catch (IOException e) {
                    Log.e(TAG, "failed to initialize speech recognition", e);
                }
                return null;
            }
        }.execute();
    }

    private void cameraSetup() {
        final SurfaceView cameraView = (SurfaceView) findViewById(R.id.surfaceView);
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                Face.this.cameraService = CameraService.builder().surfaceView(
                        cameraView).photoDirectory(
                        getExternalFilesDir(Environment.DIRECTORY_PICTURES)).build();
                if (Face.this.cameraService.available()) {
                    //photo taking needs more work and a destination HA-4
                    knowPhotoTaking();
                }
                return null;
            }
        }.execute();
    }

    public class SlackCommunicator extends Communicator {

        private SlackSession session;
        private SlackChannel channel;

        public SlackCommunicator(final String channelName, final String apiToken) {
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... params) {
                    session = SlackSessionFactory.createWebSocketSlackSession(apiToken);
                    try {
                        session.connect();
                        channel = session.findChannelByName(channelName);
                        if (channel == null) {
                            communicator.warn(getClass(),
                                    "I can't find the channel " + channelName + ". Set your location to match a Slack channel.");
                        } else {
                            robotName = session.sessionPersona().getUserName();
                            //send the message directly to know it is working
                            try {
                                final SlackMessageHandle handle = session.sendMessage(
                                        channel,
                                        robotName + " has entered the building",
                                        null);
                                Face.this.slackConnected = true;
                            } catch (Exception e) {
                                session = null;
                                communicator.error(getClass(), "I am having problems using Slack ",
                                        e);
                            }

                        }
                    } catch (IOException e) {
                        session = null;
                        communicator.error(SlackCommunicator.class, "Slack session failed", e);

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    setupSpeechInput();
                    session.addMessagePostedListener(new SlackMessagePostedListener() {
                        @Override
                        public void onEvent(SlackMessagePosted event, SlackSession session) {
                            final String messageContent = event.getMessageContent();

                            communicator.verbose(SlackCommunicator.class,
                                    messageContent + " received from " + event.getSender().getUserName());
                            if (messageContent != null && !event.getSender().isBot()) {
                                handleMessage(messageContent);
                            }
                        }


                    });
                }
            }.execute();


        }


        @Override
        public void write(@Nonnull Class<?> aClass, final Type type, final @Nullable String message, @Nullable Throwable throwable) {
            if (session != null) {
                new AsyncTask<String, Void, Void>() {
                    @Override
                    protected Void doInBackground(String... params) {
                        if (channel != null && type.ordinal() >= Type.INFO.ordinal()) {
                            try {
                                session.sendMessage(channel, message, null);
                            } catch (Exception e) {
                                //communicating the problem causes an infinite loop
                                Log.e(getClass().getSimpleName(),
                                        "problems communicating with " + channel, e);
                            }

                        }
                        return null;
                    }
                }.execute();
            }
        }


    }

    /**
     * The main way to handle messages received from users.
     *
     * @param messageContent
     */
    private void handleMessage(final String messageContent) {
        try {
            //query the knowledge base
            Question question = new SimpleQuestion(messageContent);
            final ListenableFuture<Answer> answerFuture = knowledgeBase.answer(question);
            Futures.addCallback(answerFuture, new FutureCallback<Answer>() {
                @Override
                public void onSuccess(@Nullable Answer answer) {

                    if (Answers.isNotEmpty(answer)) {
                        communicator.info(getClass(), answer.toString());
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    String failureMessage = "Sorry, I'm having problems responding to " + messageContent;
                    if (t != null) {
                        failureMessage += " because " + t.getMessage();
                    }
                    communicator.error(getClass(), failureMessage, t);

                }
            });


        } catch (Throwable e) {
            communicator.warn(getClass(),
                    "Sorry.  Something bad happened: " + e.getMessage());
            Log.e(Face.class.getSimpleName(), "error invoked by message " + messageContent,
                    e);
        }
    }

    /**
     * Navigation that does not require localization first.
     */
    private void knowNavigation() {

        final KnownAnswers.KnownAnswersBuilder knownAnswersBuilder = KnownAnswers.builder().subject(
                "navigate coordinates");
        //go to coordinate
        {
            final TrailingSubjectQuestion goToQuestion = new TrailingSubjectQuestion(
                    "go to coordinate ");
            final Function<Question, Object[]> goToFunction = new Function<Question, Object[]>() {
                @Nullable
                @Override
                public Object[] apply(@Nullable Question input) {
                    final String requestedName = goToQuestion.subject(input);
                    Object[] results;
                    if (StringUtils.isEmpty(requestedName)) {
                        results = new Object[]{new SimpleAnswer("Go to which coordinates?")};
                    } else {

                        //maybe it's a coordinate
                        final Coordinate coordinate = JtsFormat.parseCoordinate(requestedName);
                        if (coordinate != null) {
                            goToCoordinate(requestedName, coordinate);
                            results = new Object[]{JtsFormat.brief(coordinate)};
                        } else {
                            results = ResultsAnswer.alternateAnswer(
                                    "I don't understand the coordinates of " + requestedName);
                        }

                    }
                    return results;
                }
            };

            ResultsAnswer goToAnswer = ResultsAnswer.builder("Going to coordinates %s").function(
                    goToFunction).question(goToQuestion).build();
            knownAnswersBuilder.answer(goToQuestion, goToAnswer);
        }

        //go forward x.x meters or go forward x.x m or go forward x.x
        {
            final TrailingSubjectQuestion question = TrailingSubjectQuestion.builder(
                    "go forward ").subject("meters").build();
            final Function<Question, Object[]> function = new Function<Question, Object[]>() {
                @Nullable
                @Override
                public Object[] apply(@Nullable Question input) {
                    final String subject = question.subject(input);
                    if (StringUtils.isNotEmpty(subject)) {
                        final String[] parts = StringUtils.split(subject, StringUtils.SPACE);
                        final String valuePart = parts[0];

                        try {
                            final double meters = Double.parseDouble(valuePart);
                            motionService.navigator(HeadingNavigator.forward(meters));
                            return ResultsAnswer.results(
                                    NavigationObservationTypes.distanceFormatted(
                                            meters));
                        } catch (NumberFormatException e) {
                            return ResultsAnswer.alternateAnswer(
                                    valuePart + " must be a number in meters");
                        }
                    } else {
                        return ResultsAnswer.alternateAnswer("provide the distance in meters");
                    }
                }


            };

            ResultsAnswer answer = ResultsAnswer.builder("going forward %s").question(
                    question).function(
                    function).build();
            knownAnswersBuilder.answer(question, answer);
        }
        //where are you
        {
            SimpleQuestion question = new SimpleQuestion("where are you");
            final ResultsAnswer answer = ResultsAnswer.builder(
                    "I am %s meters away from %s").question(question).supplier(
                    new Supplier<Object[]>() {

                        @Override
                        public Object[] get() {
                            Object[] results;
                            final Coordinate currentLocation = Face.this.localizer.coordinate(
                                    DateTime.now());
                            if (currentLocation != null) {
                                Place place = Face.this.mapService.nearestPlace(currentLocation);

                                if (place != null) {

                                    final double distance = place.point().getCoordinate().distance(
                                            currentLocation);
                                    results = ResultsAnswer.results(
                                            TangoDepthCalculator.METER_FORMAT.format(
                                                    distance), place.name());
                                } else {
                                    results = ResultsAnswer.alternateAnswer(
                                            "You should mark some places so I can know where I am located within " + locationName);
                                }
                            } else {
                                results = ResultsAnswer.alternateAnswer("I am currently lost");
                            }
                            return results;
                        }
                    }).build();
            knownAnswersBuilder.answer(question, answer);
        }

        know(knownAnswersBuilder.build());
    }

    private void goToCoordinate(String requestedName, Coordinate coordinate) {
        Place coordinatePlace = Place.builder().name(requestedName).point(
                new GeometryFactory().createPoint(
                        coordinate)).build();
        motionService.navigator(PlaceNavigator.builder().destination(
                coordinatePlace).build());
    }

    private void knowTango() {
        SimpleQuestion question = new SimpleQuestion("localize");
        Supplier<Object[]> supplier = new Supplier<Object[]>() {
            @Override
            public Object[] get() {
                discover();
                return new Object[0];
            }
        };
        final ResultsAnswer answer = ResultsAnswer.builder().question(question).supplier(
                supplier).build();
        know(KnownAnswers.builder().subject("use tango").answer(question, answer).build());
    }

    /**
     * Navigation after localization has happened.
     */
    private void knowMapNavigation() {
        final KnownAnswers.KnownAnswersBuilder knownAnswersBuilder = KnownAnswers.builder();
        {
            final TrailingSubjectQuestion markQuestion = TrailingSubjectQuestion.builder(
                    "mark ").subject(MapService.PLACE_SUBJECT).build();
            Function<Question, Object[]> markFunction = new Function<Question, Object[]>() {
                @Nullable
                @Override
                public Object[] apply(@Nullable Question question) {

                    Object[] results;
                    String placeName = markQuestion.subject(question);
                    final Coordinate coordinate = Face.this.localizer.coordinate(
                            DateTime.now());
                    String coordinateString;
                    if (coordinate != null) {
                        final Place place = Face.this.mapService.place(
                                placeName,
                                coordinate);
                        placeName = place.name();
                        coordinateString = JtsFormat.brief(place.point().getCoordinate());
                        results = new Object[]{placeName, coordinateString};
                    } else {
                        results = new Object[]{new SimpleAnswer(
                                "I don't know where I'm located so I can't create a new place")};
                    }
                    return results;
                }
            };

            final ResultsAnswer markAnswer = ResultsAnswer.builder(
                    "Added %s to the map at %s").function(
                    markFunction).question(markQuestion).build();
            knownAnswersBuilder.answer(
                    markQuestion, markAnswer);
        }
        if (CollectionUtils.isNotEmpty(this.mapService.places())) {


            {
                final TrailingSubjectQuestion distanceToQuestion = this.mapService.placeQuestion(
                        "what is your distance to ");
                final Function<Question, Object[]> distanceToFunction = new Function<Question, Object[]>() {
                    @Nullable
                    @Override
                    public Object[] apply(@Nullable Question input) {

                        Object[] results;
                        final String placeName = distanceToQuestion.subject(input);
                        final Place place = Face.this.mapService.place(placeName);
                        if (place != null) {
                            final Coordinate coordinate = Face.this.localizer.coordinate();
                            if (coordinate != null) {
                                results = ResultsAnswer.results(placeName,
                                        MapService.metersFormatted(
                                                place.point().getCoordinate().distance(
                                                        coordinate)));
                            } else {
                                results = ResultsAnswer.results(
                                        new SimpleAnswer("current position is not know"));
                            }
                        } else {
                            results = ResultsAnswer.results(
                                    new SimpleAnswer(placeName + " is not known"));
                        }
                        return results;
                    }
                };
                ResultsAnswer distanceToAnswer = ResultsAnswer.builder(
                        "distance to %s is %s").function(
                        distanceToFunction).question(distanceToQuestion).build();
                knownAnswersBuilder.answer(distanceToQuestion, distanceToAnswer);
            }
        }
        KnownAnswers answers = knownAnswersBuilder.subject("position on the map").build();
        KnowledgeBase.mutator(this.knowledgeBase).know(answers).build();
    }

    public void knowPhotoTaking() {
        KnownAnswers.KnownAnswersBuilder answers = KnownAnswers.builder().subject("take photos");

        //take a photo and save it
        {
            SimpleQuestion question = new SimpleQuestion("take a photo");
            ResultsAnswer answer = ResultsAnswer.builder("photo available at %s").question(
                    question).supplier(
                    new Supplier<Object[]>() {
                        @Override
                        public Object[] get() {
                            if (cameraService.available()) {
                                final ListenableFuture<File> listenableFuture = cameraService.forwardSnapshot();
                                try {
                                    return ResultsAnswer.results(listenableFuture.get());
                                } catch (Exception e) {
                                    return ResultsAnswer.alternateAnswer(
                                            "failed taking photo because " + e.getMessage());
                                }
                            } else {
                                return ResultsAnswer.alternateAnswer("camera is not available");
                            }
                        }
                    }).build();
            answers.answer(question, answer);
        }
        know(answers.build());
    }

    public void knowSlackSetup() {
        final TrailingSubjectQuestion tokenQuestion = TrailingSubjectQuestion.builder(
                "your slack bot api token is ").subject("token").build();
        Function<Question, Object[]> tokenFunction = new Function<Question, Object[]>() {
            @Nullable
            @Override
            public Object[] apply(@Nullable Question input) {
                final String apiToken = tokenQuestion.subject(input);
                getPreferences(MODE_PRIVATE).edit().putString(SLACKBOT_API_TOKEN_KEY,
                        apiToken).commit();
                communicatorsInitialized();
                return new String[]{robotName, locationName};
            }
        };
        ResultsAnswer tokenAnswer = ResultsAnswer.builder(
                "my name is %s. you may now communicate with me on the slack channel %s").function(
                tokenFunction).question(tokenQuestion).build();

        final TrailingSubjectQuestion locationQuestion = TrailingSubjectQuestion.builder(
                "you are located in ").subject("location").build();
        Function<Question, Object[]> locationFunction = new Function<Question, Object[]>() {
            @Nullable
            @Override
            public Object[] apply(@Nullable Question input) {


                locationName = locationQuestion.subject(input);
                getPreferences(MODE_PRIVATE).edit().putString(LOCATION_NAME_KEY,
                        locationName).commit();
                //setup communications with the new location name
                communicatorsInitialized();
                return new Object[]{locationName};
            }
        };
        ResultsAnswer locationAnswer = ResultsAnswer.builder(
                "I will learn and take care of %s").function(locationFunction).question(
                locationQuestion).build();

        KnownAnswers answers = KnownAnswers.builder().subject("setup slack").answer(tokenQuestion,
                tokenAnswer).answer(locationQuestion, locationAnswer).build();
        know(answers);
    }


    private class SpeechInput implements RecognitionListener {
        final SpeechRecognizer speechRecognizer;

        SpeechInput() {
            if (SpeechRecognizer.isRecognitionAvailable(Face.this)) {
                speechRecognizer = SpeechRecognizer.createSpeechRecognizer(
                        Face.this);
                speechRecognizer.setRecognitionListener(this);
                know(knowledgeable());
            } else {
                this.speechRecognizer = null;
                Log.w(TAG, "speech recognition is unavailable");
            }
        }


        public Knowledgeable knowledgeable() {
            KnownAnswers.KnownAnswersBuilder answers = KnownAnswers.builder().subject(
                    "listen");
            {
                SimpleQuestion question = new SimpleQuestion("listen");
                ResultsAnswer answer = ResultsAnswer.builder("i'm listening").question(
                        question).supplier(
                        new Supplier<Object[]>() {
                            @Override
                            public Object[] get() {
                                listen();
                                return new Object[0];
                            }
                        }).build();
                answers.answer(question, answer);
            }
            return answers.build();
        }

        private void listen() {

            //listening must be done on the main thread
            //http://stackoverflow.com/questions/11123621/running-code-in-main-thread-from-another-thread
            Handler mainHandler = new Handler(Looper.getMainLooper());

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                            "voice.recognition.test");

                    intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
                    speechRecognizer.startListening(intent);
                }
            };
            mainHandler.post(runnable);

        }

        private void stopListening() {
            speechRecognizer.stopListening();
        }

        @Override
        public void onReadyForSpeech(Bundle params) {
            Log.v(TAG, "ready for speech");

        }

        @Override
        public void onBeginningOfSpeech() {
            Log.v(TAG, "beginning of speech");
        }

        @Override
        public void onRmsChanged(float rmsdB) {
            Log.v(TAG, "rms changed");
        }

        @Override
        public void onBufferReceived(byte[] buffer) {
            Log.v(TAG, "buffer received");
        }

        @Override
        public void onEndOfSpeech() {
            Log.v(TAG, "end of speech");
        }

        @Override
        public void onError(int error) {
            Log.v(TAG, "error " + error);
        }

        @Override
        public void onResults(Bundle results) {
            final ArrayList<String> list = results.getStringArrayList(
                    SpeechRecognizer.RESULTS_RECOGNITION);
            final float[] confidences = results.getFloatArray(SpeechRecognizer.CONFIDENCE_SCORES);

            Log.v(TAG, "responded with " + list + " with " + confidences);
            if (CollectionUtils.isNotEmpty(list)) {
                handleMessage(list.get(0));
            }
            stopListening();
        }

        @Override
        public void onPartialResults(Bundle partialResults) {
            Log.v(TAG, "partial results " + partialResults);
        }

        @Override
        public void onEvent(int eventType, Bundle params) {
            Log.v(TAG, "event type " + eventType + " with " + params);
        }
    }
}
