package com.aawhere.tango.jts;

import com.google.atap.tangoservice.TangoPoseData;
import com.vividsolutions.jts.geom.Coordinate;

import org.apache.commons.lang3.NotImplementedException;

/**
 * Common functions to translate Tango data structures to JTS.
 * <p/>
 * Created by aroller on 7/2/15.
 */
public class TangoJtsUtil {

    /**
     * Converts tango pose data into a JTS coordinate.
     * <p/>
     * Project Tango uses a right-handed, local-level frame for the START_OF_SERVICE and
     * AREA_DESCRIPTION coordinate frames. This convention sets the Z-axis aligned with gravity,
     * with Z+ pointed upwards, and the X-Y plane is perpendicular to gravity that is locally level
     * with the ground plane.
     * <p/>
     * https://developers.google.com/project-tango/overview/frames-of-reference
     * <p/>
     * <p/>
     * http://mathworld.wolfram.com/Right-HandRule.html
     * <p/>
     * The coordinate system given from the pose data behaves similar to the geo coordinates. So x =
     * longitude, y = latitutde and z = altitude.
     *
     * @param tangoPoseData
     * @return
     */
    public static Coordinate coordinate(TangoPoseData tangoPoseData) {
        if (tangoPoseData.baseFrame == TangoPoseData.COORDINATE_FRAME_AREA_DESCRIPTION || tangoPoseData.baseFrame == TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE) {
            double[] translation = tangoPoseData.translation;
            return new Coordinate(translation[TangoPoseData.INDEX_TRANSLATION_X],
                    translation[TangoPoseData.INDEX_TRANSLATION_Y],
                    translation[TangoPoseData.INDEX_TRANSLATION_Z]);
        } else {
            throw new NotImplementedException(
                    "only ADF and Start of service handled: " + tangoPoseData.baseFrame);
        }
    }
}
