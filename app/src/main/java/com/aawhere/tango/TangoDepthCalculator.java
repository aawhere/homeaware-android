package com.aawhere.tango;

import com.aawhere.lang.ObjectBuilder;
import com.google.atap.tangoservice.TangoXyzIjData;

import java.nio.FloatBuffer;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Given the PointCloud data this will look at the directly in front of the tablet for a possible
 * obstruction.
 * <p/>
 * <p/>
 * FIXME: Currently this targets a small square in the middle, but it should be more of a band that
 * is ~ 2 dm wide. Created by aroller on 7/8/15.
 */
public class TangoDepthCalculator {


    /**
     * @deprecated use NavigationObservationTypes
     * <p/>
     * Formats distance to it's relevant precision.  Arguably not appropriate to be here.
     */
    @Deprecated
    public static final NumberFormat METER_FORMAT;

    static {
        METER_FORMAT = DecimalFormat.getInstance();
        METER_FORMAT.setMaximumFractionDigits(2);
        METER_FORMAT.setMinimumFractionDigits(2);
        METER_FORMAT.setMinimumIntegerDigits(1);
    }

    private Double averageDistanceInMeters;
    private Double minDistanceInMeters;
    private Integer numberOfPointsOfInterest;
    private Integer numberOfPointsInCloud;
    private Integer minPointCountForConfident = 1000;

    /**
     * Used to construct all instances of TangoDepthCalculator.
     */
    public static class TangoDepthCalculatorBuilder extends ObjectBuilder<TangoDepthCalculator> {


        private Double xCoordinateMax = 0.20;
        private Double yCoordinateMax = 0.020;
        private TangoXyzIjData xyzIjData;

        private TangoDepthCalculatorBuilder() {
            super(new TangoDepthCalculator());
        }


        public TangoDepthCalculatorBuilder data(TangoXyzIjData data) {
            this.xyzIjData = data;
            return this;
        }

        public TangoDepthCalculator build() {
            TangoDepthCalculator built = super.build();
            built.numberOfPointsInCloud = xyzIjData.xyzCount;

            final FloatBuffer xyz = xyzIjData.xyz;
            double cumulativeZ = 0.0;
            int numberOfPointsOfInterest = 0;
            Double minDistanceInMeters = Double.MAX_VALUE;
            for (int i = 0; i < xyzIjData.xyzCount; i += 3) {
                float x = xyz.get(i);
                float y = xyz.get(i + 1);
                if (Math.abs(x) < xCoordinateMax && Math.abs(y) < yCoordinateMax) {
                    float z = xyz.get(i + 2);
                    cumulativeZ += z;
                    numberOfPointsOfInterest++;
                    minDistanceInMeters = Math.min(minDistanceInMeters, z);
                }
            }

            Double distanceInMeters;
            if (numberOfPointsOfInterest > 0) {
                //avoid division by zero
                distanceInMeters = cumulativeZ / numberOfPointsOfInterest;
            } else {
                if (built.numberOfPointsInCloud >= built.minPointCountForConfident) {
                    distanceInMeters = Double.POSITIVE_INFINITY;
                    minDistanceInMeters = Double.POSITIVE_INFINITY;
                } else {
                    distanceInMeters = null;
                    minDistanceInMeters = null;
                }
            }
            built.averageDistanceInMeters = distanceInMeters;
            built.numberOfPointsOfInterest = numberOfPointsOfInterest;
            built.minDistanceInMeters = minDistanceInMeters;
            return built;
        }

    }//end Builder

    public static TangoDepthCalculatorBuilder builder() {
        return new TangoDepthCalculatorBuilder();
    }

    /**
     * Use {@link TangoDepthCalculatorBuilder} to construct TangoDepthCalculator
     */
    private TangoDepthCalculator() {
    }

    public Integer numberOfPointsOfInterest() {
        return numberOfPointsOfInterest;
    }

    public Double averageDistanceInMeters() {
        return averageDistanceInMeters;
    }

    /**
     * The closest point in the immediate path that was detected.
     */
    public Double minDistanceInMeters() {
        return minDistanceInMeters;
    }

    public Integer mumberOfPointsInCloud() {
        return numberOfPointsInCloud;
    }

    /**
     * Indicates there is no obstruction in the near view...from what can be seen.  WARNING:There is
     * no guarantee there is nothing in the way, but just that nothing showed up in the cloud.
     *
     * @return
     */
    public boolean isClear() {
        return minDistanceInMeters != null && minDistanceInMeters.isInfinite();
    }

    /**
     * Indicates if the results given should be relied upon.
     */
    public boolean isConfident() {
        return minDistanceInMeters != null;
    }

}
