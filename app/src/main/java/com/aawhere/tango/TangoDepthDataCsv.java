package com.aawhere.tango;

import com.aawhere.lang.ObjectBuilder;
import com.google.atap.tangoservice.TangoXyzIjData;
import com.google.common.base.Predicate;
import com.google.common.io.Closeables;

import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Translates the depth data into a csv given of only x,y,z
 * <p/>
 * Created by aroller on 9/24/15.
 */
public class TangoDepthDataCsv {


    private IOException error;
    private File file;

    /**
     * Used to construct all instances of TangoDepthDataCsv.
     */
    public static class TangoDepthDataCsvBuilder extends ObjectBuilder<TangoDepthDataCsv> {
        private TangoXyzIjData data;
        private String name;
        private File directory;


        private TangoDepthDataCsvBuilder() {
            super(new TangoDepthDataCsv());
        }

        public TangoDepthDataCsvBuilder data(TangoXyzIjData data) {
            this.data = data;
            return this;
        }

        public TangoDepthDataCsvBuilder directory(File file) {
            this.directory = file;
            return this;
        }

        public TangoDepthDataCsvBuilder name(String name) {
            this.name = name;
            return this;
        }

        public TangoDepthDataCsv build() {
            TangoDepthDataCsv built = super.build();
            File file;
            {
                int suffix = 1;
                do {
                    file = new File(directory, name + suffix++ + ".csv");
                } while (file.exists());
            }
            built.file = file;
            try {
                FileWriter csv = new FileWriter(file);
                char comma = ',';
                final String newLine = System.lineSeparator();
                csv.append("i,x,y,z");
                csv.append(newLine);
                NumberFormat format = DecimalFormat.getInstance();
                format.setMaximumFractionDigits(3);
                format.setMinimumFractionDigits(3);
                format.setMinimumIntegerDigits(1);
                format.setGroupingUsed(false);
                final FloatBuffer xyz = data.xyz;


                for (int i = 0; i < data.xyzCount; i += 3) {
                    float x = xyz.get(i);
                    float y = xyz.get(i + 1);
                    float z = xyz.get(i + 2);
                    csv.append(String.valueOf((i / 3) + 1));
                    csv.append(comma);
                    csv.append(format.format(x));
                    csv.append(comma);
                    csv.append(format.format(y));
                    csv.append(comma);
                    csv.append(format.format(z));
                    csv.append(newLine);
                }
                Closeables.close(csv, false);
            } catch (IOException e) {
                built.error = e;

            }

            return built;
        }

    }//end Builder

    public static TangoDepthDataCsvBuilder builder() {
        return new TangoDepthDataCsvBuilder();
    }

    /**
     * Use {@link TangoDepthDataCsvBuilder} to construct TangoDepthDataCsv
     */
    private TangoDepthDataCsv() {
    }

    /**
     * An error indicates a failure in writing the file and explains why.
     *
     * @return
     */
    public IOException error() {
        return error;
    }

    public File file() {
        return file;
    }
}
