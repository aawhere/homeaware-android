package com.aawhere.tango;

import android.support.annotation.Nullable;

import com.aawhere.measure.QuaternionEulerAngles;
import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.TangoAreaDescriptionMetaData;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.vividsolutions.jts.util.CollectionUtil;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

/**
 * Created by aroller on 7/4/15.
 */
public class TangoUtil {

    public static final boolean MATCH_WITH_EQUALS = false;
    public static final boolean MATCH_WITH_CONTAINS = true;

    /**
     * Converts the tango pose data into the QuaternionEulerAngles capable of solving heading only
     * (about the y axis or "yaw").
     *
     * @param tangoPoseData
     * @return
     */
    public static QuaternionEulerAngles headingAngles(TangoPoseData tangoPoseData) {
        return quaternionEulerAnglesBuilder(tangoPoseData).headingOnly().build();
    }

    public static QuaternionEulerAngles.QuaternionEulerAnglesBuilder quaternionEulerAnglesBuilder(TangoPoseData tangoPoseData) {
        return QuaternionEulerAngles.builder().w(
                tangoPoseData.rotation[TangoPoseData.INDEX_ROTATION_W]).x(
                tangoPoseData.rotation[TangoPoseData.INDEX_ROTATION_X]).y(
                tangoPoseData.rotation[TangoPoseData.INDEX_ROTATION_Y]).z(
                tangoPoseData.rotation[TangoPoseData.INDEX_ROTATION_Z]);
    }

    /**
     * Filter used to find a Tango ADF with a name that matches the desired.
     *
     * @param lookingForName
     * @param matchWithContains
     * @return
     */
    public static Predicate<TangoAreaDescriptionMetaData> namePredicate(@Nonnull final String lookingForName, final boolean matchWithContains) {
        return new Predicate<TangoAreaDescriptionMetaData>() {
            @Override
            public boolean apply(TangoAreaDescriptionMetaData input) {
                //FIXME: This should be extracted to a nameFunction and use existing Predicates.contains and Predicates.equals
                String name = name(input);
                if (matchWithContains) {
                    return (name != null) ? name.contains(lookingForName) : false;
                } else {
                    return (name != null) ? name.equalsIgnoreCase(lookingForName) : false;
                }
            }
        };
    }

    public static Function<String, TangoAreaDescriptionMetaData> metaDataFunction(final Tango tango) {
        return new Function<String, TangoAreaDescriptionMetaData>() {
            @javax.annotation.Nullable
            @Override
            public TangoAreaDescriptionMetaData apply(@Nullable String uuid) {
                return tango.loadAreaDescriptionMetaData(uuid);
            }
        };
    }

    public static List<TangoAreaDescriptionMetaData> areaDescriptionMetaDatas(final Tango tango) {
        final ArrayList<String> areaDescriptions = tango.listAreaDescriptions();
        return Lists.transform(areaDescriptions, metaDataFunction(tango));
    }

    /**
     * Retrieves name from metadata if available.
     */
    @Nullable
    private static String name(TangoAreaDescriptionMetaData input) {
        final byte[] nameBytes = input.get(TangoAreaDescriptionMetaData.KEY_NAME);
        String name;
        if (nameBytes != null) {
            name = new String(nameBytes);
        } else {
            name = null;
        }
        return name;
    }

    public static String uuid(TangoAreaDescriptionMetaData tangoAreaDescriptionMetaData) {
        return new String(tangoAreaDescriptionMetaData.get(TangoAreaDescriptionMetaData.KEY_UUID));
    }


}
